<?php

declare(strict_types=1);

namespace Tests\Integration;

use Aws\CacheInterface;
use NewImmoGroup\AwsBroker\ArrayCache;
use NewImmoGroup\AwsBroker\Payload\AttributeDict;
use NewImmoGroup\AwsBroker\Payload\QueueAttribute;
use NewImmoGroup\AwsBroker\Payload\SqsFacade;
use PHPUnit\Framework\TestCase;

class SqsFacadeTest extends TestCase
{
    private const GENERIC_QUEUE_NAME = 'messenger-transport-sqs-integration';
    private SqsFacade $facade;
    private static CacheInterface $cache;

    public function purgeExistingMessages(): void
    {
        do {
            $existingMessages = $this->facade->requestReceiveMessage();
            $counter = 0;
            foreach ($existingMessages as $message) {
                $this->facade->requestDeleteMessage($message->receiptHandle);
                ++$counter;
            }
        } while ($counter >= 10);
    }

    public static function setUpBeforeClass(): void
    {
        self::$cache = new ArrayCache();
    }

    protected function setUp(): void
    {
        $this->facade = new SqsFacade(
            $region = SqsFacade::getEnvAsString('AWS_REGION'),
            SqsFacade::getEnvAsString('AWS_VERSION'),
            $accountId = SqsFacade::getEnvAsString('AWS_ACCOUNT_ID'),
            [],
            self::$cache,
            $queueName = self::GENERIC_QUEUE_NAME.'-'.$this->getName(),
            SqsFacade::makeQueueUrl($region, $accountId, $queueName),
        )
        ;
    }

    public function makePartOfQueueName(): string
    {
        $kebabCase = preg_replace_callback(
            '/[A-Z]/',
            fn (array $match): string => '-'.strtolower($match[0]),
            $this->getName()
        );
        assert(is_string($kebabCase));

        return $kebabCase;
    }

    private function deleteQueueIfExists(): void
    {
        if (null !== $this->facade->requestListQueueTagsIfQueueExists()) {
            $this->facade->requestDeleteQueue();
        }
    }

    public function testCreatingQueue(): void
    {
        $this->deleteQueueIfExists();
        $created = $this->facade->requestCreateQueue([], new AttributeDict());
        self::assertSame($this->facade->queueUrl, $created->queueUrl);
        $this->facade->requestDeleteQueue();
        self::assertNull($this->facade->requestListQueueTagsIfQueueExists(), 'Created queue was not deleted');
    }

    public function testForcingAttributes(): void
    {
        $this->facade->createDefaultQueueIfNotExists();

        $response = $this->facade->requestGetQueueAttributes([QueueAttribute::DELAY_SECONDS]);
        self::assertNotSame('17', $response->attributes->DelaySeconds ?? 'not-in-payload');

        $attributes = new AttributeDict();
        $attributes->DelaySeconds = '17';
        $this->facade->requestSetQueueAttributes($attributes);

        $response = $this->facade->requestGetQueueAttributes([QueueAttribute::DELAY_SECONDS]);
        self::assertSame('17', $response->attributes->DelaySeconds ?? 'not-in-payload');

        // cleanup
        $attributes->DelaySeconds = '10';
        $this->facade->requestSetQueueAttributes($attributes);
    }

    public function testIdempotentTags(): void
    {
        $this->facade->createDefaultQueueIfNotExists();

        self::assertArrayNotHasKey('team', $this->facade->requestListQueueTags()->tags);
        $this->facade->requestTagQueue(['team' => 'beta']);
        self::assertSame('beta', $this->facade->requestListQueueTags()->tags['team']);
        $this->facade->requestUntagQueue(['team']);
        self::assertArrayNotHasKey('team', $this->facade->requestListQueueTags()->tags);
    }

    public function testSendMessage(): void
    {
        $this->facade->createDefaultQueueIfNotExists();

        $this->purgeExistingMessages();

        $this->facade->requestSendMessage('my-body', null);
        /** @var list<\NewImmoGroup\AwsBroker\Payload\ReceiveMessage\Response> $messages */
        $messages = iterator_to_array($this->facade->requestReceiveMessage());
        self::assertNotEmpty($messages);
        $message = array_shift($messages);
        self::assertEmpty($messages);
        self::assertNotNull($message);
        $this->facade->requestDeleteMessage($message->receiptHandle);
        /** @var list< \NewImmoGroup\AwsBroker\Payload\ReceiveMessage\Response> $messages */
        $messages = iterator_to_array($this->facade->requestReceiveMessage());
        self::assertEmpty($messages);
    }
}
