<?php

declare(strict_types=1);

namespace Tests\Unit;

use NewImmoGroup\AwsBroker\Entity\Configuration;
use NewImmoGroup\AwsBroker\Exception\InvalidParameterException;
use NewImmoGroup\AwsBroker\Exception\MissingParameterException;
use PHPUnit\Framework\TestCase;

class ConfigurationTest extends TestCase
{
    public function testGetQueueUrl(): void
    {
        // GIVEN
        $dsn = 'https://host/path';
        $options = [
            'queue_name' => 'queue-name',
        ];

        // WHEN
        $configuration = new Configuration(
            $dsn,
            $options
        );

        // THEN
        $this->assertSame(
            $dsn.'/'.$options['queue_name'],
            $configuration->getQueueUrl()
        );
    }

    public function testGetQueueUrlWithInvalidDSN(): void
    {
        // GIVEN
        $dsn = 'https://host';
        $options = [
            'queue_name' => 'queue-name',
        ];

        // WHEN
        $configuration = new Configuration(
            $dsn,
            $options
        );

        // THEN
        $this->expectException(InvalidParameterException::class);
        $configuration->getQueueUrl();
    }

    public function testGetQueueUrlWhenQueueNameMissing(): void
    {
        // GIVEN
        $dsn = 'https://host/path';
        $options = [];

        // WHEN
        $configuration = new Configuration(
            $dsn,
            $options
        );

        // THEN
        $this->expectException(MissingParameterException::class);
        $configuration->getQueueUrl();
    }

    public function testGetAutoSetupNotDefined(): void
    {
        // GIVEN
        $options = [];

        // WHEN
        $configuration = new Configuration(
            '',
            $options
        );

        // THEN
        $this->assertFalse(
            $configuration->hasAutoSetup()
        );
    }

    public function testGetAutoSetupEnabled(): void
    {
        // GIVEN
        $options = [
            'auto_setup' => true,
        ];

        // WHEN
        $configuration = new Configuration(
            '',
            $options
        );

        // THEN
        $this->assertTrue(
            $configuration->hasAutoSetup()
        );
    }

    public function testGetOptions(): void
    {
        // GIVEN
        $options = [
            'hip' => 'hop',
            'pif' => 'paf',
            'zig' => 'zag',
        ];

        // WHEN
        $configuration = new Configuration(
            '',
            $options
        );

        // THEN
        $this->assertSame(
            $options,
            $configuration->getOptions()
        );
    }
}
