<?php

declare(strict_types=1);

namespace Tests\Unit\SqsFacadeHelper;

use Generator;
use NewImmoGroup\AwsBroker\Payload\AttributeDict;
use NewImmoGroup\AwsBroker\Payload\CreateQueue;
use NewImmoGroup\AwsBroker\Payload\DeleteMessage;
use NewImmoGroup\AwsBroker\Payload\DeleteQueue;
use NewImmoGroup\AwsBroker\Payload\GetQueueAttributes;
use NewImmoGroup\AwsBroker\Payload\ListQueueTags;
use NewImmoGroup\AwsBroker\Payload\SendMessage;
use NewImmoGroup\AwsBroker\Payload\SetQueueAttributes;
use NewImmoGroup\AwsBroker\Payload\SqsFacadeInterface;
use NewImmoGroup\AwsBroker\Payload\TagQueue;
use NewImmoGroup\AwsBroker\Payload\UntagQueue;

final class InMemoryFacade implements SqsFacadeInterface
{
    /**
     * @var array<\NewImmoGroup\AwsBroker\Payload\ReceiveMessage\Response>
     */
    public array $msgs = [];

    public function requestCreateQueue(array $tags, AttributeDict $dict): CreateQueue\Response
    {
        return new CreateQueue\Response('queue-url');
    }

    public function requestGetQueueAttributes(array $attributes): GetQueueAttributes\Response
    {
        return new GetQueueAttributes\Response(new AttributeDict());
    }

    public function requestListQueueTagsIfQueueExists(): ?ListQueueTags\Response
    {
        return new ListQueueTags\Response(['tag1' => 'value', 'tag2' => 'value']);
    }

    public function requestListQueueTags(): ListQueueTags\Response
    {
        return new ListQueueTags\Response(['tag1' => 'value', 'tag2' => 'value']);
    }

    public function requestSetQueueAttributes(AttributeDict $attributes): SetQueueAttributes\Response
    {
        return new SetQueueAttributes\Response();
    }

    public function requestDeleteQueue(): DeleteQueue\Response
    {
        return new DeleteQueue\Response();
    }

    public function requestTagQueue(array $tags): TagQueue\Response
    {
        return new TagQueue\Response();
    }

    public function requestUntagQueue(array $tagKeys): UntagQueue\Response
    {
        return new UntagQueue\Response();
    }

    public function requestReceiveMessage(): Generator
    {
        yield from $this->msgs;
    }

    public function requestSendMessage(string $body, ?int $delay): SendMessage\Response
    {
        return new SendMessage\Response('12345');
    }

    public function requestSendMessageForceQueueUrl(string $body, ?int $delay, string $queueUrl): SendMessage\Response
    {
        return new SendMessage\Response('12345');
    }

    public function requestDeleteMessage(string $receiptHandle): DeleteMessage\Response
    {
        return new DeleteMessage\Response();
    }

    public function createDefaultQueueIfNotExists(): void
    {
    }

    public function idempotentQueueCreation(array $tags, AttributeDict $dict): void
    {
    }

    public function getQueueUrl(): string
    {
        return 'https://queue.url';
    }
}
