<?php

declare(strict_types=1);

namespace Tests\Unit\SqsFacadeHelper;

use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Transport\Serialization\SerializerInterface;

final class InMemorySerializer implements SerializerInterface
{
    /**
     * @var array<numeric-string, Envelope>
     */
    public array $envelopes = [];

    /**
     * @param array{id: numeric-string} $encodedEnvelope
     */
    public function decode(array $encodedEnvelope): Envelope
    {
        return $this->envelopes[$encodedEnvelope['id']];
    }

    /**
     * @return array{id: numeric-string}
     */
    public function encode(Envelope $envelope): array
    {
        return ['id' => '18'];
    }
}
