<?php

declare(strict_types=1);

namespace Tests\Unit;

use NewImmoGroup\AwsBroker\Entity\Stamp;
use PHPUnit\Framework\TestCase;

class StampTest extends TestCase
{
    public function testStamp(): void
    {
        // GIVEN
        $queueUrl = 'test-queue-url';
        $receiptHandle = 'test-receipt-handle';

        // WHEN
        $stamp = new Stamp($queueUrl, $receiptHandle);

        // THEN
        $this->assertSame($queueUrl, $stamp->getQueueUrl());
        $this->assertSame($receiptHandle, $stamp->getReceiptHandle());
    }
}
