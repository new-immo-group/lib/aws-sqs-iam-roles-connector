<?php

declare(strict_types=1);

namespace Tests\Unit;

use NewImmoGroup\AwsBroker\Entity\Stamp;
use NewImmoGroup\AwsBroker\Event\NullEventDispatcher;
use NewImmoGroup\AwsBroker\Payload\ReceiveMessage\Response;
use NewImmoGroup\AwsBroker\Payload\SqsTransportOption;
use NewImmoGroup\AwsBroker\Payload\SqsTransportUsingFacade;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Messenger\Envelope;
use Tests\Unit\SqsFacadeHelper\InMemoryFacade;
use Tests\Unit\SqsFacadeHelper\InMemorySerializer;
use Tests\Unit\SqsFacadeHelper\TestMessage;

final class SqsTransportUsingFacadeTest extends TestCase
{
    private SqsTransportUsingFacade $transport;
    private InMemoryFacade $facade;
    private SqsTransportOption $options;
    private InMemorySerializer $serializer;

    public function setUp(): void
    {
        $this->facade = new InMemoryFacade();
        $this->options = new SqsTransportOption();
        $this->serializer = new InMemorySerializer();
        $this->transport = new SqsTransportUsingFacade(
            $this->facade,
            $this->serializer,
            new NullEventDispatcher(),
            $this->options
        );
    }

    public function testReceiveMessage(): void
    {
        $this->facade->msgs = [
            new Response('1234', '{"id": "18"}', 'receipt-this-url'),
        ];

        $this->options->autoSetup = false;
        $this->serializer->envelopes['18'] = new Envelope(new TestMessage());
        $msgsIterable = $this->transport->get();
        assert(!is_array($msgsIterable));
        $msgs = iterator_to_array($msgsIterable);

        self::assertEquals([new Envelope(new TestMessage(), [new Stamp(
            $this->facade->getQueueUrl(),
            'receipt-this-url',
        )])], $msgs);
    }
}
