<?php

declare(strict_types=1);

namespace Tests\Unit;

use Aws\Sqs\Exception\SqsException;
use NewImmoGroup\AwsBroker\SqsAdapter;
use NewImmoGroup\AwsBroker\SqsSender;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Exception\TransportException;
use Symfony\Component\Messenger\Transport\Serialization\PhpSerializer;
use Symfony\Component\Messenger\Transport\Serialization\SerializerInterface;
use Tests\Functional\Command\MessageOne;

class SqsSenderTest extends TestCase
{
    private SerializerInterface $serializer;

    public function setUp(): void
    {
        $this->serializer = new PhpSerializer();

        parent::setUp();
    }

    public function testSendMessage(): void
    {
        // GIVEN
        $adapter = $this->createMock(SqsAdapter::class);
        $sender = new SqsSender(
            $adapter,
            $this->serializer
        );

        $envelope = new Envelope(
            new MessageOne('test-id')
        );

        // ASSERT
        $adapter
            ->expects($this->once())
            ->method('sendMessage');

        // WHEN
        $sender->send($envelope);
    }

    public function testSendMessageToNonExistentQueueAndAutoSetupDisabled(): void
    {
        // GIVEN
        $adapter = $this->createMock(SqsAdapter::class);

        $adapter
            ->expects($this->once())
            ->method('hasAutoSetup')
            ->willReturn(false);
        $adapter
            ->expects($this->once())
            ->method('sendMessage')
            ->willThrowException(
                $this->mockSqsNonExistentQueueException()
            );

        $sender = new SqsSender(
            $adapter,
            $this->serializer
        );

        $envelope = new Envelope(
            new MessageOne('test-id')
        );

        // ASSERT
        $this->expectException(TransportException::class);

        // WHEN
        $sender->send($envelope);
    }

    public function testSendMessageToNonExistentQueueAndAutoSetupEnabled(): void
    {
        // GIVEN
        $adapter = $this->createMock(SqsAdapter::class);
        $adapter
            ->expects($this->once())
            ->method('hasAutoSetup')
            ->willReturn(true);
        $adapter
            ->expects($this->exactly(2))
            ->method('sendMessage')
            ->willReturnOnConsecutiveCalls(
                $this->throwException($this->mockSqsNonExistentQueueException()),
                'queue-created-and-message-sent-ok'
            );

        $sender = new SqsSender(
            $adapter,
            $this->serializer
        );

        $envelope = new Envelope(
            new MessageOne('test-id')
        );

        // ASSERT
        $adapter
            ->expects($this->once())
            ->method('createQueue');

        // WHEN
        $sender->send($envelope);
    }

    public function testSendMessageWhenCreateQueueFails(): void
    {
        // GIVEN
        $adapter = $this->createMock(SqsAdapter::class);
        $adapter
            ->expects($this->once())
            ->method('hasAutoSetup')
            ->willReturn(true);
        $adapter
            ->expects($this->once())
            ->method('sendMessage')
            ->willThrowException(
                $this->mockSqsNonExistentQueueException()
            );
        $adapter
            ->expects($this->once())
            ->method('createQueue')
            ->willThrowException(
                $this->mockSqsException()
            );

        $sender = new SqsSender(
            $adapter,
            $this->serializer
        );

        $envelope = new Envelope(
            new MessageOne('test-id')
        );

        // ASSERT
        $this->expectException(TransportException::class);

        // WHEN
        $sender->send($envelope);
    }

    private function mockSqsNonExistentQueueException(): SqsException
    {
        $exception = $this->createMock(SqsException::class);
        $exception
            ->expects($this->once())
            ->method('getAwsErrorCode')
            ->willReturn(
                'AWS.SimpleQueueService.NonExistentQueue'
            );

        return $exception;
    }

    private function mockSqsException(): SqsException
    {
        return $this->createMock(SqsException::class);
    }
}
