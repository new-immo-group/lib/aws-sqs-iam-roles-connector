<?php

declare(strict_types=1);

namespace Tests\Functional;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Tester\CommandTester;
use Tests\Functional\Command\MessageOneHandler;
use Tests\Functional\Command\MessageTwoHandler;

class E2ETest extends KernelTestCase
{
    public function testSendAndReceiveMessages(): void
    {
        // GIVEN
        $kernel = self::bootKernel();
        $application = new Application($kernel);

        // THEN
        $this->testSendAndConsumeMessageOne($application);
        $this->testSendAndConsumeMessageTwo($application);
    }

    private function testSendAndConsumeMessageOne(
        Application $application
    ): void {
        $messageSenderCommand = $application->find('app:send:one');
        $sendMessage = new CommandTester($messageSenderCommand);

        $sentId = 'watch-for-this-'.random_int(0, 100);

        $sendMessage->execute(['id' => [$sentId]]);

        // When sending a message
        $consumeMessagesCommand = $application->find('messenger:consume');

        $consumeTester = new CommandTester($consumeMessagesCommand);

        $consumeTester->execute(
            ['receivers' => ['test_transport_one'], '--limit' => 1],
            [
                'verbosity' => OutputInterface::VERBOSITY_VERY_VERBOSE,
                'interactive' => true,
                'capture_stderr_separately' => false,
            ]
        );

        // Then MessageHandler as found received a message
        // This way of testing is awful
        // But I couldn't get $consumeTester to provide me with MessageHandler's outputs
        // Be it via stdout, stderr or log
        self::assertSame(
            sprintf("This is the content of the message one: %s\n", $sentId),
            MessageOneHandler::getMessage()
        );
    }

    private function testSendAndConsumeMessageTwo(
        Application $application
    ): void {
        $messageSenderCommand = $application->find('app:send:two');
        $sendMessage = new CommandTester($messageSenderCommand);

        $sentId = 'look-for-this-'.random_int(0, 100);

        $sendMessage->execute(['id' => [$sentId]]);

        // When sending a message
        $consumeMessagesCommand = $application->find('messenger:consume');

        $consumeTester = new CommandTester($consumeMessagesCommand);

        $consumeTester->execute(
            ['receivers' => ['test_transport_two'], '--limit' => 1],
            [
                'verbosity' => OutputInterface::VERBOSITY_VERY_VERBOSE,
                'interactive' => true,
                'capture_stderr_separately' => false,
            ]
        );

        // Then MessageHandler as found received a message
        // This way of testing is awful
        // But I couldn't get $consumeTester to provide me with MessageHandler's outputs
        // Be it via stdout, stderr or log
        self::assertSame(
            sprintf("This is the content of the message two: %s\n", $sentId),
            MessageTwoHandler::getMessage()
        );
    }
}
