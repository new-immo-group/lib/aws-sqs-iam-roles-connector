<?php

declare(strict_types=1);

namespace Tests\Functional\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class SendMessageOne extends Command
{
    protected static $defaultName = 'app:send:one';

    private MessageBusInterface $messageBus;

    public function __construct(MessageBusInterface $messageBus)
    {
        parent::__construct();
        $this->messageBus = $messageBus;
    }

    protected function configure(): void
    {
        $this->addArgument(
            'id',
            InputArgument::REQUIRED | InputArgument::IS_ARRAY,
            'id du message'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        foreach ($this->extract($input) as $id) {
            $this->messageBus->dispatch(new MessageOne($id));
        }

        return Command::SUCCESS;
    }

    /**
     * @return string[]
     */
    protected function extract(InputInterface $input): array
    {
        /** @var array<string> $array */
        $array = $input->getArgument('id');

        return $array;
    }
}
