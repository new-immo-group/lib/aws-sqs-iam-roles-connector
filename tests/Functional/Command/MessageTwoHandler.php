<?php

declare(strict_types=1);

namespace Tests\Functional\Command;

use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class MessageTwoHandler implements MessageHandlerInterface
{
    private static ?string $message = null;
    private LoggerInterface $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public static function getMessage(): ?string
    {
        return self::$message;
    }

    public function __invoke(MessageTwo $message): void
    {
        self::$message = sprintf("This is the content of the message two: %s\n", $message->id);
        $this->logger->info(self::$message);
    }
}
