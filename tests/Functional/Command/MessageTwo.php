<?php

declare(strict_types=1);

namespace Tests\Functional\Command;

class MessageTwo
{
    public string $id;

    public function __construct(string $id)
    {
        $this->id = $id;
    }
}
