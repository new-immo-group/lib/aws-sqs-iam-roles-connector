<?php

declare(strict_types=1);

namespace Tests\Functional\Command;

use JsonException;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class MessageOneNormalizer implements NormalizerInterface, DenormalizerInterface
{
    /**
     * @param string               $data
     * @param array<string, mixed> $context
     *
     * @throws JsonException
     */
    public function denormalize($data, string $type, ?string $format = null, array $context = []): MessageOne
    {
        /** @var MessageOne $code */
        $code = json_decode($data, false, 512, JSON_THROW_ON_ERROR);

        return new MessageOne($code->id);
    }

    /**
     * @param array<string, mixed> $data
     * @param array<string, mixed> $context
     */
    public function supportsDenormalization($data, string $type, ?string $format = null, array $context = []): bool
    {
        return MessageOne::class === $type;
    }

    /**
     * @param object               $object
     * @param array<string, mixed> $context
     *
     * @return string
     *
     * @throws JsonException
     */
    public function normalize($object, ?string $format = null, array $context = [])
    {
        return json_encode($object, JSON_THROW_ON_ERROR);
    }

    /**
     * @param object               $data
     * @param array<string, mixed> $context
     */
    public function supportsNormalization($data, ?string $format = null, array $context = []): bool
    {
        return 'json' === $format && $data instanceof MessageOne;
    }
}
