## [2.0.6](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/compare/v2.0.5...v2.0.6) (2025-02-10)


### Refactoring

* make a facade to handle all calls ([7d5210d](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/7d5210dd7aa37e3559d3533c464e16f64fc2c2f1))

## [2.0.5](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/compare/v2.0.4...v2.0.5) (2025-01-31)


### Refactoring

* create payloads for send, recv and delete message ([0934621](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/0934621b7251d058793fcec925d722283accebe0))

## [2.0.4](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/compare/v2.0.3...v2.0.4) (2025-01-31)


### Refactoring

* payloads queue tags ([a5c5f57](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/a5c5f57a070040007315a7bf33502709c96581fe))

## [2.0.3](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/compare/v2.0.2...v2.0.3) (2025-01-31)


### Refactoring

* payloads for forcing attributes ([84cd09e](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/84cd09e5b83b025f5fcda7fa7689edff20b7ef44))

## [2.0.2](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/compare/v2.0.1...v2.0.2) (2025-01-30)


### Refactoring

* new architecture to communicate with sqs ([8620b11](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/8620b11f1a2a1d7a7c6d32bc7add508d0443b417))

## [2.0.1](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/compare/v2.0.0...v2.0.1) (2024-12-17)


### Bug Fixes

* add a http timeout in case connexion goes bad ([373b7da](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/373b7daa7a716b05881a8c364d7628d995378d2a))

## [1.12.0](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/compare/v1.11.0...v1.12.0) (2024-09-13)


### ⚠ BREAKING CHANGES

* Making a major release

### Features

* auto-create queue when getting msg ([d316371](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/d316371d7a9b2dc16cadd0054629e5c2cc7a5bff))


### Documentation

* sqs permissions ([3cea5e2](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/3cea5e2e20fc236f3b408c0cc40e514245c959a3))


### Chores

* BETA-6547 - create queue from message destination queue if provided ([d3a3d28](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/d3a3d28fe5e7eaba1db4aa4d523329786571648b))
* check for an existing queue beforehand ([5cf6802](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/5cf6802f13b9c37973f6c37fc016d4729011602b))
* Making a major release ([a1d0eb2](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/a1d0eb2602e57a8ed9f22be3b0b055bed4c26ee0))
* **release:** release 1.12.0 ([fd25624](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/fd256244d9c498944b8a9e1880e991024449bb3d))
* **release:** release 1.12.1 ([45cc8b5](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/45cc8b54f60180c75c2b2bdaecfc48488b0a304f))

## [1.12.1](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/compare/v1.12.0...v1.12.1) (2024-09-13)


### Documentation

* sqs permissions ([3cea5e2](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/3cea5e2e20fc236f3b408c0cc40e514245c959a3))

## [1.12.0](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/compare/v1.11.0...v1.12.0) (2024-09-09)


### Features

* auto-create queue when getting msg ([d316371](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/d316371d7a9b2dc16cadd0054629e5c2cc7a5bff))


### Chores

* BETA-6547 - create queue from message destination queue if provided ([d3a3d28](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/d3a3d28fe5e7eaba1db4aa4d523329786571648b))

## [1.11.0](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/compare/v1.10.1...v1.11.0) (2024-08-21)


### Features

* SqsReceiver dispatches events when decoding message ([ccf5fca](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/ccf5fcaae7ed8e836e27bd4c32ad1d9fd1b637b8))

## [1.10.1](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/compare/v1.10.0...v1.10.1) (2024-08-08)


### Refactoring

* do not reclone envelope ([7fea02a](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/7fea02acf64a7b16e5a3af33bb19f96799cb5ee9))

## [1.10.0](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/compare/v1.9.0...v1.10.0) (2024-08-08)


### Features

* actually respect requested delay ([8cdd02e](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/8cdd02e3e264673f4a51dd589e2bf23e556da43e))

## [1.9.0](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/compare/v1.8.1...v1.9.0) (2024-08-08)


### Features

* get number of avail messages ([c410fda](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/c410fda93b668a36293fe33b60848cc1d305a3e6))


### Bug Fixes

* error in phpstan file ([457376b](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/457376b3de1ce1994f43c0d10e4c063e0471e8ea))

## [1.8.1](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/compare/v1.8.0...v1.8.1) (2024-07-01)


### Bug Fixes

* use getter because client might not be initialised at this point ([93208c6](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/93208c63195576a3e98df9b072132c2a51f18ce8))

## [1.8.0](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/compare/v1.7.0...v1.8.0) (2024-04-09)


### Features

* make sqs transport setupable ([cab2c36](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/cab2c36a88244ef47ef16058f51e1feeeebcda0d))
* tester in lib ([7dad6b3](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/7dad6b3eeb31867e5b7539cb86f3682af5b12910))


### Refactoring

* only use queuename if necessary ([c3a763e](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/c3a763ed4640f1fc8bcfe4c3b025ec3e5c1d16d2))

## [1.7.0](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/compare/v1.6.0...v1.7.0) (2024-02-29)


### Features

* add visibility timeout ([0acc5e1](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/0acc5e16caafb88b0d7d334a741fb2e888a87707))

## [1.6.0](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/compare/v1.5.0...v1.6.0) (2024-02-29)


### Features

* add a static simple array cache to cred retrieval ([5870b1b](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/5870b1b55600bf2bf4851e4bfec0911e4b63a1b3))
* allow mixed to avoid triggering deprecation notices ([a434687](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/a434687a8a4c4e11303aa8bebd69933c362fc927))
* use defaultProvider ([39a30c7](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/39a30c70d2a279b1890145c69aedf43633cc83de))

## [1.5.0](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/compare/v1.4.0...v1.5.0) (2024-02-27)


### Features

* expose queue-name in transport ([33f201f](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/33f201f60fa58bfe5a790703cedc004e5e956b83))

## [1.4.0](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/compare/v1.3.0...v1.4.0) (2024-02-27)


### Features

* better src fmt ([b11ac2c](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/b11ac2cec82fd1904017c3e2045cbcf5e9cccea6))

## [1.3.0](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/compare/v1.2.2...v1.3.0) (2024-02-26)


### Features

* reply-to queue url ([90a3d23](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/90a3d231329a09fa8029da8747d5e6f667c44020))

## [1.2.2](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/compare/v1.2.1...v1.2.2) (2024-02-26)


### Refactoring

* fix normalizers ([7cd546c](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/7cd546c61c81b20dff885a40f4c1531518681a91))

## [1.2.1](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/compare/v1.2.0...v1.2.1) (2023-11-14)


### Chores

* ack lesser symfo versions ([d9f8810](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/d9f881025dc12c54a3d056dece0b12fa18021e5c))

## [1.2.0](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/compare/v1.1.11...v1.2.0) (2023-07-25)


### Features

* Allow configuring the maximum amount of message to fetch at once ([7dee550](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/7dee550ea5fe0f66906d2d98a889dfedcf06286b))
* Allow waiting time configuration when retrieving messages ([4e5939f](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/4e5939fd43f792736cde859bcff0f523a9916775))

## [1.1.11](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/compare/v1.1.10...v1.1.11) (2023-07-11)


### Bug Fixes

* Ignore cache folder ([c0412e6](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/c0412e6d1d57624264b43cec7b82d3c9bed778a4))
* Ignore every test file when creating archive using .gitattributes ([aa8bcb2](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/aa8bcb2ee60d3d66a9e4ea7ca21473fae7b9a020))

## [1.1.10](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/compare/v1.1.9...v1.1.10) (2023-06-28)


### Chores

* **deps:** update dependency new-immo-group/devops/ci-cd/template to v8.23.1 ([209de17](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/209de17b422d52ab66b11ed8ac2f933d052fc103))

## [1.1.9](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/compare/v1.1.8...v1.1.9) (2023-05-11)


### Bug Fixes

* messenger allowed required version ([ec26f7c](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/ec26f7c37b3c4bba5babc444d6a648b0803349a2))

## [1.1.8](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/compare/v1.1.7...v1.1.8) (2023-05-11)


### Bug Fixes

* allow greater psr/log versions ([0b45bbf](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/0b45bbf095f5acfa14f2b8a6f90cdce406a91167))

## [1.1.7](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/compare/v1.1.6...v1.1.7) (2023-05-11)


### Bug Fixes

* update supported php version in composer json ([eac34d1](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/eac34d1af4f7d5229748525cdaff4e9472c5e221))

## [1.1.6](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/compare/v1.1.5...v1.1.6) (2023-04-16)


### Chores

* **deps:** update dependency new-immo-group/devops/ci-cd/template to v8.21.1 ([6c64eed](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/6c64eed1402f5413337bb22db90eeb4b91d2eff8))

## [1.1.5](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/compare/v1.1.4...v1.1.5) (2023-03-19)


### Chores

* **deps:** update dependency new-immo-group/devops/ci-cd/template to v8.21.0 ([b28f677](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/b28f677fb18a23569b2558d13cfa56f8969c26fd))

## [1.1.4](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/compare/v1.1.3...v1.1.4) (2023-03-15)


### Bug Fixes

* queue_prefix in E2E configuration ([609765a](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/609765adf72fc716da1c5286a3c3667e8ccfca49))
* rename  to ([4d63329](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/4d633298e98773622dc3b4d4420cd39406ce525f))
* reviews ([ff9e220](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/ff9e220bc9d7fbecacc219d78d77e2d744f9392c))


### Tests

* modify E2E test to make sure transport-factory instanciation bug stay gone ([e70188d](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/e70188d1c21711b582cc748a3eb368c480448275))
* modify E2E test to make sure transport-factory instanciation bug stay gone (new test message) ([47e3ce1](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/47e3ce17699b135ab35c7e61ae7732198efe3cf6))


### Documentation

* fix typos ([080cc92](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/080cc92842024d71d44ec648676f6d103eb7a387))
* updated README ([5c8b1ee](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/5c8b1ee0e7477ba74113466ae6be7c06a5f0beeb))


### Refactoring

* rename api to adapter in unit-test ([44502c9](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/44502c96d90fd4f10ba1dc350b0f1b0b44faf1ed))
* rewrite transport-factory api/client instanciation and add queue-tags support ([c826db2](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/c826db226d15d3012913de0fece5ab5aca59dfa0))

## [1.1.3](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/compare/v1.1.2...v1.1.3) (2023-03-12)


### Chores

* **deps:** update dependency new-immo-group/devops/ci-cd/template to v8.19.5 ([e1cb2a6](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/e1cb2a647d4687803f1ca88d70e35d0579d8c9cb))

## [1.1.2](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/compare/v1.1.1...v1.1.2) (2023-03-05)


### Chores

* **deps:** update dependency new-immo-group/devops/ci-cd/template to v8.19.3 ([bf40c04](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/bf40c0412d45bbb372e1c78ce993979532f1095c))

## [1.1.1](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/compare/v1.1.0...v1.1.1) (2023-02-26)


### Chores

* **deps:** update dependency new-immo-group/devops/ci-cd/template to v8.19.0 ([fe4698e](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/fe4698ed57288b1535a63a363ba81e9db709be06))

## [1.1.0](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/compare/v1.0.30...v1.1.0) (2023-02-20)


### Features

* add support for queue prefix ([85758b9](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/85758b9fa1bc99df99e73bbee1e245841fe2bd1a))

## [1.0.30](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/compare/v1.0.29...v1.0.30) (2023-02-19)


### Chores

* **deps:** update dependency new-immo-group/devops/ci-cd/template to v8.16.2 ([6f877ab](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/6f877ab3159625c64f818aeea5961a40f03b2818))

## [1.0.29](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/compare/v1.0.28...v1.0.29) (2023-02-12)


### Chores

* **deps:** update dependency new-immo-group/devops/ci-cd/template to v8.12.1 ([197d6fd](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/197d6fd17af7be30a15f464e8f57d15f7d37ffa4))

## [1.0.28](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/compare/v1.0.27...v1.0.28) (2023-02-05)


### Chores

* **deps:** update dependency new-immo-group/devops/ci-cd/template to v8.10.0 ([f17451f](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/f17451f39f04ed54d2f4ff30ccf1e51dc6c65931))

## [1.0.27](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/compare/v1.0.26...v1.0.27) (2023-01-29)


### Chores

* **deps:** update dependency new-immo-group/devops/ci-cd/template to v8.9.0 ([5108258](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/5108258cdb3f8a7da06ef25b9e700c3760bc4908))

## [1.0.26](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/compare/v1.0.25...v1.0.26) (2023-01-22)


### Chores

* **deps:** update dependency new-immo-group/devops/ci-cd/template to v8.6.5 ([9ea1153](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/9ea1153ce86131d73b21d02970847be8b71e72f8))

## [1.0.25](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/compare/v1.0.24...v1.0.25) (2023-01-15)


### Chores

* **deps:** update dependency new-immo-group/devops/ci-cd/template to v8.5.1 ([a762f0b](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/a762f0b62b8070e86144b6add26b562114bf2981))

## [1.0.24](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/compare/v1.0.23...v1.0.24) (2023-01-08)


### Chores

* **deps:** update dependency new-immo-group/devops/ci-cd/template to v8.5.0 ([8abfbee](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/8abfbeecbf6c2b366da942197821dbdde3d41f3f))

## [1.0.23](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/compare/v1.0.22...v1.0.23) (2023-01-01)


### Chores

* **deps:** update dependency new-immo-group/devops/ci-cd/template to v8.4.0 ([1c7c890](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/1c7c8909c3eabdf43637c6f804c68d409a7e6d88))

## [1.0.22](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/compare/v1.0.21...v1.0.22) (2022-12-25)


### Chores

* **deps:** update dependency new-immo-group/devops/ci-cd/template to v8.3.2 ([77e3f1f](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/77e3f1ff19967ac18cea4618a746d83fc9de6953))

## [1.0.21](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/compare/v1.0.20...v1.0.21) (2022-12-18)


### Chores

* **deps:** update dependency new-immo-group/devops/ci-cd/template to v8.3.0 ([736734a](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/736734a1a6c92581ebc4337398d0a7f511c7d671))

## [1.0.20](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/compare/v1.0.19...v1.0.20) (2022-12-12)


### Bug Fixes

* add missing [@throws](https://gitlab.com/throws) tags ([0012595](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/0012595bbc876699cab7922972487e302f43f066))
* proper [@throws](https://gitlab.com/throws) tags in SqsReceiver ([cea5462](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/cea54629f2e62417422e3837c5ebd889e22fa4e6))
* proper [@throws](https://gitlab.com/throws) tags in SqsReceiver ([a9cb018](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/a9cb018b64466f719997ede09eafb152fbb79051))
* remove useless comments in tests ([16d12e6](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/16d12e66b287eb85c858b63f93c8430de217d66e))


### Refactoring

* delete poc and rewrite transport for production use ([2474baf](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/2474baf31be1fd55b34f410e35d080e67952ce22))
* delete poc and rewrite transport for production use ([694834e](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/694834ee1f7119c2c56119e40f3ed6a031baea22))
* factorized utility method isQueueNotFoundException ([d903acf](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/d903acf26dfec7fca9da8b0ffcba416744566d85))
* factorized utility method isQueueNotFoundException ([fde6ccd](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/fde6ccd471d80a74f3b263c4e760886f79f1576c))
* replace if-not with if in SqsReceiver ([b649d4c](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/b649d4c5e1b979bd4323411aade37d68e5c8e712))
* replace if-not with if in SqsReceiver ([2204771](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/22047712896e4f6da2953c2624c531e55aaaa6bc))


### Documentation

* update README ([18fc97a](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/18fc97adca46707db9b278b1cdf969af0a42896a))
* update README ([81b5615](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/81b56153643deac911047a79a375349f76a6c5b8))
* update README about open_basedir() ([9c15286](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/9c152863eaf0025061c7067b6bc199d7f92b0590))


### Chores

* bump template ref to v8.0.1 ([f268678](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/f26867831d0bc4a35dd156d29777c8368f2b74ae))
* **composer:** update sf6 composer.lock ([27e4816](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/27e4816fcc5e19eedd586eabae12ef452fc09a14))
* **composer:** update sf6 composer.lock ([1f73e6c](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/1f73e6cdae5b14aac8058f71d9b0b0b5c8136337))
* **env:** remove useless comments ([4f41308](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/4f41308647e88cb7eea64533d2137e17df965bc9))
* **env:** remove useless comments ([17ddee3](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/17ddee393d9d67737f18095499bf3d44c03a9f01))
* **phpcsfixer:** pimp ruleset ([d5af74d](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/d5af74d8287680bc8dcad8d26e1913f4dd012469))
* **phpcsfixer:** pimp ruleset ([74f2980](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/74f2980238ad763984a1e6372cf1b2036de77b48))

## [1.0.19](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/compare/v1.0.18...v1.0.19) (2022-12-04)


### Chores

* **deps:** update dependency new-immo-group/devops/ci-cd/template to v7.9.2 ([033fa3a](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/033fa3ad2e865874e55d68a0aae3130dff761c78))

## [1.0.18](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/compare/v1.0.17...v1.0.18) (2022-12-02)


### Bug Fixes

* keep empty test-unit folder ([7173182](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/717318211a506e5364ca8f6437a5a6934c813aa8))


### Documentation

* update README ([d6abdf2](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/d6abdf2ebb543f604d79e78de0add8702b865ef5))


### Chores

* **ci:** bump template ref ([0658ee4](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/0658ee4d77d921f2751fb70f723a2e92b08f7ff7))
* **composer-require-checker:** installed for both sf5 and sf6 environments ([8c60086](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/8c60086f3cebb63ae67c8c181f1c3f1ff125df11))
* **composer:** setup testing for php7/8 and sf5/6 ([a13aaac](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/a13aaacbb8e84fe9ee42adedd0a3479cf19f36d9))
* **tests:** create unit and functional testsuites ([1f9e3f9](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/1f9e3f90c2ca24acc7c4072500b71998c9d0bb75))

## [1.0.17](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/compare/v1.0.16...v1.0.17) (2022-11-21)


### Refactoring

* rename library ([da3db59](https://gitlab.com/new-immo-group/lib/messenger-transport-sqs-iam/commit/da3db5910c876944794f5bd10abcf7c318d8ecb3))

## [1.0.16](https://gitlab.com/new-immo-group/lib/aws-sqs-iam-roles-connector/compare/v1.0.15...v1.0.16) (2022-11-20)


### Chores

* **deps:** update dependency new-immo-group/devops/ci-cd/template to v7.7.4 ([0d2d60a](https://gitlab.com/new-immo-group/lib/aws-sqs-iam-roles-connector/commit/0d2d60ae7f91a5e2b86da056e51a8c7fc3f449b9))

## [1.0.15](https://gitlab.com/new-immo-group/lib/aws-sqs-iam-roles-connector/compare/v1.0.14...v1.0.15) (2022-11-13)


### Chores

* **deps:** update dependency new-immo-group/devops/ci-cd/template to v7.7.2 ([26bfdb5](https://gitlab.com/new-immo-group/lib/aws-sqs-iam-roles-connector/commit/26bfdb55418aa60617bdffcdd0aeacc30c83217e))

## [1.0.14](https://gitlab.com/new-immo-group/lib/aws-sqs-iam-roles-connector/compare/v1.0.13...v1.0.14) (2022-11-06)


### Chores

* **deps:** update dependency new-immo-group/devops/ci-cd/template to v7.7.1 ([1ac76dd](https://gitlab.com/new-immo-group/lib/aws-sqs-iam-roles-connector/commit/1ac76ddfeb036515c287eefbb11c48c1e19c5490))

## [1.0.13](https://gitlab.com/new-immo-group/lib/aws-sqs-iam-roles-connector/compare/v1.0.12...v1.0.13) (2022-10-31)


### Chores

* **deps:** update dependency new-immo-group/devops/ci-cd/template to v7 ([ba22520](https://gitlab.com/new-immo-group/lib/aws-sqs-iam-roles-connector/commit/ba22520e5b4548c2c1201846db065af5292dbe4d))

## [1.0.12](https://gitlab.com/new-immo-group/lib/aws-sqs-iam-roles-connector/compare/v1.0.11...v1.0.12) (2022-10-30)


### Chores

* **deps:** update dependency new-immo-group/devops/ci-cd/template to v5.9.2 ([b9adbc2](https://gitlab.com/new-immo-group/lib/aws-sqs-iam-roles-connector/commit/b9adbc287c9266c9ab78e650e3142de4741d4af6))

## [1.0.11](https://gitlab.com/new-immo-group/lib/aws-sqs-iam-roles-connector/compare/v1.0.10...v1.0.11) (2022-10-25)


### Chores

* **renovate:** enable automerge for ci-cd templates ([7a64361](https://gitlab.com/new-immo-group/lib/aws-sqs-iam-roles-connector/commit/7a64361ebb9582419fe7be13054a991e9dfc7980))

## [1.0.10](https://gitlab.com/new-immo-group/lib/aws-sqs-iam-roles-connector/compare/v1.0.9...v1.0.10) (2022-10-20)


### Bug Fixes

* does not replace symfony/polyfill* packages ([ebb601a](https://gitlab.com/new-immo-group/lib/aws-sqs-iam-roles-connector/commit/ebb601a8d81c8f681d75b555e8ad83adfc5e763d))
* remove auto-scripts ([862a59b](https://gitlab.com/new-immo-group/lib/aws-sqs-iam-roles-connector/commit/862a59b376833fa582a158e3732efb9a4a895be8))
* set correct PHP required version to the tested ones ([4a568f4](https://gitlab.com/new-immo-group/lib/aws-sqs-iam-roles-connector/commit/4a568f402cd7e37327226f584b558741a720d382))


### Chores

* Remove unnecessary Composer packages ([b668f95](https://gitlab.com/new-immo-group/lib/aws-sqs-iam-roles-connector/commit/b668f95ad718e40de1c4dfe6ae9a0717baa842aa))


### Documentation

* Add composer require command ([03906bf](https://gitlab.com/new-immo-group/lib/aws-sqs-iam-roles-connector/commit/03906bf226bfa4cfb5e31d2721230989c62bf1c4))

## [1.0.9](https://gitlab.com/new-immo-group/lib/aws-sqs-iam-roles-connector/compare/v1.0.8...v1.0.9) (2022-10-10)


### Chores

* **deps:** update dependency new-immo-group/devops/ci-cd/template to v5.4.0 ([aadef8a](https://gitlab.com/new-immo-group/lib/aws-sqs-iam-roles-connector/commit/aadef8a28903236d20876334ceeae4f753d96011))

## [1.0.8](https://gitlab.com/new-immo-group/lib/aws-sqs-iam-roles-connector/compare/v1.0.7...v1.0.8) (2022-10-03)


### Chores

* **deps:** update dependency new-immo-group/devops/ci-cd/template to v5.1.2 ([3198c7b](https://gitlab.com/new-immo-group/lib/aws-sqs-iam-roles-connector/commit/3198c7b3023a8f2aa4b30a345525230aa3e89f48))

## [1.0.7](https://gitlab.com/new-immo-group/lib/aws-sqs-iam-roles-connector/compare/v1.0.6...v1.0.7) (2022-09-30)


### Bug Fixes

* symfony is a test dep ([f26fecc](https://gitlab.com/new-immo-group/lib/aws-sqs-iam-roles-connector/commit/f26fecc12b373e8131dd737f2833d35d0b61ac75))

## [1.0.6](https://gitlab.com/new-immo-group/lib/aws-sqs-iam-roles-connector/compare/v1.0.5...v1.0.6) (2022-09-26)


### Bug Fixes

* remove polyfill ([8b99755](https://gitlab.com/new-immo-group/lib/aws-sqs-iam-roles-connector/commit/8b99755cc56b1c18c239aff1a800133ec2ae38e4))

## [1.0.5](https://gitlab.com/new-immo-group/lib/aws-sqs-iam-roles-connector/compare/v1.0.4...v1.0.5) (2022-09-26)

## [1.0.4](https://gitlab.com/new-immo-group/lib/aws-sqs-iam-roles-connector/compare/v1.0.3...v1.0.4) (2022-09-23)

## [1.0.3](https://gitlab.com/new-immo-group/lib/aws-sqs-iam-roles-connector/compare/v1.0.2...v1.0.3) (2022-09-20)

## [1.0.2](https://gitlab.com/new-immo-group/lib/aws-sqs-iam-roles-connector/compare/v1.0.1...v1.0.2) (2022-09-12)

## [1.0.1](https://gitlab.com/new-immo-group/lib/aws-sqs-iam-roles-connector/compare/v1.0.0...v1.0.1) (2022-09-05)

# 1.0.0 (2022-08-29)


### Bug Fixes

* **ci:** ./bin/phpunit in install deps ([422239a](https://gitlab.com/new-immo-group/lib/aws-sqs-iam-roles-connector/commit/422239a1b3e6ab0b6c1953fb89e35b417fca1791))
* **ci:** composer must be ran with php 7.4 ([e8e697c](https://gitlab.com/new-immo-group/lib/aws-sqs-iam-roles-connector/commit/e8e697c2afdef4f66dd008176de10b9f44662d89))
* **ci:** make lib work from ECS account service ([208aa24](https://gitlab.com/new-immo-group/lib/aws-sqs-iam-roles-connector/commit/208aa24d5d382bdd2562bdeda51b83ffe868eed1))
* composer require checker should declare memory limit ([dbab7ae](https://gitlab.com/new-immo-group/lib/aws-sqs-iam-roles-connector/commit/dbab7aed3142dafe0de57a9e74f384ae64758772))
* is not project ([7e21325](https://gitlab.com/new-immo-group/lib/aws-sqs-iam-roles-connector/commit/7e2132559b031b60898c5ac0358767c8c6fb8d2b))
* is not proprietary ([298ccb4](https://gitlab.com/new-immo-group/lib/aws-sqs-iam-roles-connector/commit/298ccb4aaf9ef0566c3bc4d92042a822395bb5a4))
* phpstan should boostrap phpunit ([d8a5b2d](https://gitlab.com/new-immo-group/lib/aws-sqs-iam-roles-connector/commit/d8a5b2dd6248a54a319cb48d036521a4130ccce0))
* try with correct queue prefix ([f1a1fb8](https://gitlab.com/new-immo-group/lib/aws-sqs-iam-roles-connector/commit/f1a1fb81e33db91829029e1f6b534cb5b5cb7772))


### Features

* working SQS connection ([7918638](https://gitlab.com/new-immo-group/lib/aws-sqs-iam-roles-connector/commit/7918638610bb0f090172eef84adb125968bd5ccf))
