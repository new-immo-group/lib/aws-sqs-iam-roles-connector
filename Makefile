.SILENT:
PHP7 := $(shell command -v php7.4 || echo php7.4)
PHP8 := $(shell command -v php8.1 || echo php8.1)
COMPOSER := $(shell command -v composer.phar || command -v composer || echo composer)

.PHONY: all
all: .git/hooks/commit-msg

.git/hooks/commit-msg: | .git/hooks
	cp commitlint-verification.sh $@

.PHONY: install-commitlint
install-commitlint:
	npm install --global --save-dev @commitlint/{config-conventional,cli}

.git/hooks:
	mkdir -p $@

composer.lock: composer.json | $(COMPOSER)
	$(COMPOSER) update && touch $@

.PHONY: php7cli
php7cli:
	@if [ ! -f $(PHP7) ]; then \
		echo "$(PHP7)-cli not found\n"; \
        exit 1; \
	fi

.PHONY: php8cli
php8cli:
	@if [ ! -f $(PHP8) ]; then \
		echo "$(PHP8)-cli not found\n"; \
        exit 1; \
	fi

.PHONY: test
test: composer.lock php7cli
	$(PHP7) $(COMPOSER) scenario default
	$(PHP7) $(COMPOSER) test

.PHONY: test@php8
test@php8: composer.lock php8cli
	$(PHP8) $(COMPOSER) scenario default
	$(PHP8) $(COMPOSER) test

.PHONY: test@sf6
test@sf6: composer.lock php8cli
	$(PHP8) $(COMPOSER) scenario sf6
	$(PHP8) $(COMPOSER) test
