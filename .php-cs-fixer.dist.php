<?php

$finder = (new PhpCsFixer\Finder())
    ->in(__DIR__)
    ->exclude('var');

return (new PhpCsFixer\Config())
    ->setRules(
        [
            '@Symfony' => true,
            'single_line_throw' => false,
            'method_argument_space' => ['on_multiline' => 'ensure_fully_multiline'],
            'no_superfluous_phpdoc_tags' => ['allow_unused_params' => false, 'allow_mixed' => true],
            'global_namespace_import' => [
                'import_constants' => true,
                'import_functions' => true,
                'import_classes' => true,
            ],
        ]
    )
    ->setFinder($finder);
