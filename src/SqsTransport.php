<?php

declare(strict_types=1);

namespace NewImmoGroup\AwsBroker;

use Aws\Sqs\Exception\SqsException;
use NewImmoGroup\AwsBroker\Entity\ApproximateNumberOfMessageInQueue;
use NewImmoGroup\AwsBroker\Event\EventDispatcherInterface;
use NewImmoGroup\AwsBroker\Exception\MissingParameterException;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Transport\Serialization\SerializerInterface;
use Symfony\Component\Messenger\Transport\SetupableTransportInterface;
use Symfony\Component\Messenger\Transport\TransportInterface;

class SqsTransport implements TransportInterface, SetupableTransportInterface
{
    private SerializerInterface $serializer;
    private SqsAdapter $adapter;
    private SqsSender $sender;
    private SqsReceiver $receiver;
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(
        SqsAdapter $adapter,
        EventDispatcherInterface $eventDispatcher,
        SerializerInterface $serializer
    ) {
        $this->adapter = $adapter;
        $this->serializer = $serializer;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function get(): iterable
    {
        return $this->getReceiver()->get();
    }

    public function ack(
        Envelope $envelope
    ): void {
        $this->getReceiver()->ack($envelope);
    }

    public function reject(
        Envelope $envelope
    ): void {
        $this->getReceiver()->reject($envelope);
    }

    public function send(
        Envelope $envelope
    ): Envelope {
        return $this->getSender()->send($envelope);
    }

    private function getReceiver(): SqsReceiver
    {
        return $this->receiver ??= new SqsReceiver(
            $this->adapter,
            $this->eventDispatcher,
            $this->serializer,
        );
    }

    private function getSender(): SqsSender
    {
        return $this->sender ??= new SqsSender(
            $this->adapter,
            $this->serializer,
        );
    }

    public function getQueueName(): string
    {
        return $this->adapter->getQueueName();
    }

    public static function isQueueNotFoundException(
        SqsException $exception
    ): bool {
        return 'AWS.SimpleQueueService.NonExistentQueue' === $exception->getAwsErrorCode();
    }

    public function getApproximateNumberOfMessages(): ApproximateNumberOfMessageInQueue
    {
        return $this->adapter->getApproximateNumberOfMessages();
    }

    /**
     * @throws MissingParameterException
     */
    public function setup(): void
    {
        $this->adapter->createQueue();
    }
}
