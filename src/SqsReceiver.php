<?php

declare(strict_types=1);

namespace NewImmoGroup\AwsBroker;

use Aws\Sqs\Exception\SqsException;
use LogicException;
use NewImmoGroup\AwsBroker\Entity\Stamp;
use NewImmoGroup\AwsBroker\Event\EventDispatcherInterface;
use NewImmoGroup\AwsBroker\Exception\InvalidParameterException;
use NewImmoGroup\AwsBroker\Exception\MissingParameterException;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Exception\MessageDecodingFailedException;
use Symfony\Component\Messenger\Exception\TransportException;
use Symfony\Component\Messenger\Transport\Receiver\ReceiverInterface;
use Symfony\Component\Messenger\Transport\Serialization\SerializerInterface;
use Throwable;

class SqsReceiver implements ReceiverInterface
{
    private SqsAdapter $adapter;
    private SerializerInterface $serializer;
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(
        SqsAdapter $adapter,
        EventDispatcherInterface $eventDispatcher,
        SerializerInterface $serializer
    ) {
        $this->adapter = $adapter;
        $this->serializer = $serializer;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @throws MissingParameterException      when DSN is missing the account ID
     * @throws InvalidParameterException      when DSN is not valid
     * @throws MessageDecodingFailedException when a message can not be decoded
     * @throws TransportException             when messages can not be fetched
     */
    public function get(): iterable
    {
        $this->adapter->autoSetup();
        $messages = $this->fetchMessages();

        foreach ($messages as $message) {
            yield $this->getStampedMessage($message);
        }

        return [];
    }

    /**
     * @param array{Body: string, ReceiptHandle: string} $message
     *
     * @throws MissingParameterException      when DSN is missing the account ID
     * @throws InvalidParameterException      when DSN is not valid
     * @throws MessageDecodingFailedException when a message can not be decoded
     */
    private function getStampedMessage(
        array $message
    ): Envelope {
        if (empty($message['Body']) || empty($message['ReceiptHandle'])) {
            throw new MessageDecodingFailedException(
                'Message missing Body and/or ReceiptHandle properties'
            );
        }

        return $this
            ->decode($message['Body'])
            ->with(
                new Stamp(
                    $this->adapter->getQueueUrl(),
                    $message['ReceiptHandle']
                )
            );
    }

    /**
     * @return array<array{Body: string, ReceiptHandle: string}>
     *
     * @throws MissingParameterException when DSN is missing the account ID
     * @throws InvalidParameterException when DSN is not valid
     * @throws TransportException        when message failed to be fetched
     */
    private function fetchMessages(): array
    {
        try {
            return $this->adapter->receiveMessage();
        } catch (SqsException $exception) {
            if (SqsTransport::isQueueNotFoundException($exception)) {
                return [];
            }

            throw new TransportException(
                $exception->getMessage(),
                $exception->getCode(),
                $exception
            );
        }
    }

    /**
     * @throws LogicException     when the message has no stamp
     * @throws TransportException when the message failed to be deleted
     */
    public function ack(
        Envelope $envelope
    ): void {
        $this->delete($envelope);
    }

    /**
     * @throws LogicException     when the message has no stamp
     * @throws TransportException when the message failed to be deleted
     */
    public function reject(
        Envelope $envelope
    ): void {
        $this->delete($envelope);
    }

    /**
     * @throws LogicException     when the message has no stamp
     * @throws TransportException when the message failed to be deleted
     */
    private function delete(
        Envelope $envelope
    ): void {
        $stamp = $this->getLastStamp($envelope);

        try {
            $this->adapter->deleteMessage(
                $stamp->getQueueUrl(),
                $stamp->getReceiptHandle()
            );
        } catch (SqsException $exception) {
            throw new TransportException(
                $exception->getMessage(),
                $exception->getCode(),
                $exception
            );
        }
    }

    /**
     * @throws LogicException when the message has no stamp
     */
    private function getLastStamp(
        Envelope $envelope
    ): Stamp {
        $stamp = $envelope->last(Stamp::class);
        if ($stamp instanceof Stamp) {
            return $stamp;
        }

        throw new LogicException('Envelope has no Stamp');
    }

    /**
     * @throws LogicException                 when the message body is not valid
     * @throws MessageDecodingFailedException when an Envelope failed to be decoded
     */
    private function decode(
        string $messageBody
    ): Envelope {
        try {
            $message = json_decode(
                $messageBody,
                true,
                512,
                JSON_THROW_ON_ERROR
            );

            if (!is_array($message)) {
                throw new LogicException('Invalid message body, array expected');
            }

            $this->eventDispatcher->messageDecoded($message);

            return $this->serializer->decode($message);
        } catch (Throwable $exception) {
            throw new MessageDecodingFailedException(
                'Failed to decode Envelope',
                $exception->getCode(),
                $exception
            );
        }
    }
}
