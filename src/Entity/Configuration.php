<?php

declare(strict_types=1);

namespace NewImmoGroup\AwsBroker\Entity;

use NewImmoGroup\AwsBroker\Exception\InvalidParameterException;
use NewImmoGroup\AwsBroker\Exception\MissingParameterException;

/**
 * @phpstan-type Options array{
 *     auto_setup?: bool,
 *     max_message?: int<1, 10>,
 *     queue_prefix?: string,
 *     queue_name?: string,
 *     queue_url?: string,
 *     queue_tags?: array<string, string>,
 *     region?: string,
 *     version?: string,
 *     wait_time?: int<0, 20>,
 *     visibility_timeout?: int<0, 43200>,
 *     unstable?: bool,
 * }
 */
class Configuration
{
    /**
     * @var Options
     */
    private array $options;
    private string $dsn;

    /**
     * @param Options $options
     */
    public function __construct(
        string $dsn,
        array $options
    ) {
        $this->dsn = $dsn;
        $this->options = $options;
    }

    /**
     * @throws MissingParameterException
     * @throws InvalidParameterException
     */
    public function getQueueUrl(): string
    {
        return $this->options['queue_url'] ??= $this->findQueueUrl();
    }

    /**
     * @throws MissingParameterException
     * @throws InvalidParameterException
     */
    private function findQueueUrl(): string
    {
        return $this->queueUrlForName($this->getQueueName());
    }

    /**
     * @throws MissingParameterException
     */
    public function getQueueName(): string
    {
        if (isset($this->options['queue_name'])) {
            return ($this->options['queue_prefix'] ?? '').$this->options['queue_name'];
        }

        throw new MissingParameterException('Queue name is not defined');
    }

    /** @return array<string, string> */
    public function getQueueTags(): array
    {
        return $this->options['queue_tags'] ?? [];
    }

    public function hasAutoSetup(): bool
    {
        return (bool) ($this->options['auto_setup'] ?? false);
    }

    public function getWaitTime(): int
    {
        return (int) ($this->options['wait_time'] ?? 0);
    }

    public function getMaxMessage(): int
    {
        return (int) ($this->options['max_message'] ?? 1);
    }

    /**
     * @return Options
     */
    public function getOptions(): array
    {
        return $this->options;
    }

    public function getVisibilityTimeout(): int
    {
        return $this->options['visibility_timeout'] ?? 30;
    }

    /**
     * @throws InvalidParameterException
     * @throws MissingParameterException
     */
    public function queueUrlForName(string $queueName): string
    {
        $url = parse_url($this->dsn);
        if (
            !is_array($url)
            || !isset($url['scheme'])
            || !isset($url['host'])
            || !isset($url['path'])
        ) {
            throw new InvalidParameterException('Invalid AWS messenger DSN: '.$this->dsn);
        }

        $accountId = $this->findAccountIdFromParsedUrl($url);

        return str_replace(
            $url['path'],
            sprintf(
                '/%s/%s',
                $accountId,
                $queueName
            ),
            $this->dsn
        );
    }

    /**
     * @param array{
     *     path: string,
     * } $url
     *
     * @throws MissingParameterException
     */
    public function findAccountIdFromParsedUrl(array $url): string
    {
        $path = array_filter(explode('/', $url['path']));
        if (empty($path)) {
            throw new MissingParameterException(
                'AWS Messenger DSN is missing account ID: '.$this->dsn
            );
        }

        return array_shift($path);
    }

    public function disableAutoSetup(): void
    {
        $this->options['auto_setup'] = false;
    }

    /**
     * @throws InvalidParameterException
     * @throws MissingParameterException
     */
    public function findAccountIdFromDsn(): string
    {
        $urlPath = parse_url($this->dsn, PHP_URL_PATH);
        if (!is_string($urlPath)) {
            throw new InvalidParameterException('Invalid AWS messenger DSN: '.$this->dsn);
        }

        return $this->findAccountIdFromParsedUrl(['path' => $urlPath]);
    }
}
