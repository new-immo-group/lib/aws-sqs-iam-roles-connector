<?php

declare(strict_types=1);

namespace NewImmoGroup\AwsBroker\Entity;

class ApproximateNumberOfMessageInQueue
{
    public int $available;
    public int $delayed;
    public int $invisible;

    public function __construct(int $available, int $delayed, int $invisible)
    {
        $this->available = $available;
        $this->delayed = $delayed;
        $this->invisible = $invisible;
    }
}
