<?php

declare(strict_types=1);

namespace NewImmoGroup\AwsBroker\Entity;

use Symfony\Component\Messenger\Stamp\StampInterface;

class Stamp implements StampInterface
{
    private string $queueUrl;
    private string $receiptHandle;

    public function __construct(
        string $queueUrl,
        string $receiptHandle
    ) {
        $this->queueUrl = $queueUrl;
        $this->receiptHandle = $receiptHandle;
    }

    public function getQueueUrl(): string
    {
        return $this->queueUrl;
    }

    public function getReceiptHandle(): string
    {
        return $this->receiptHandle;
    }
}
