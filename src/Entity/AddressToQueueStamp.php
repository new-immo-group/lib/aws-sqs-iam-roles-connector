<?php

declare(strict_types=1);

namespace NewImmoGroup\AwsBroker\Entity;

use Symfony\Component\Messenger\Stamp\NonSendableStampInterface;

final class AddressToQueueStamp implements NonSendableStampInterface
{
    public string $queueName;

    public function __construct(string $queueName)
    {
        $this->queueName = $queueName;
    }
}
