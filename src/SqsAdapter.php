<?php

declare(strict_types=1);

namespace NewImmoGroup\AwsBroker;

use Aws\CacheInterface;
use Aws\Credentials\CredentialProvider;
use Aws\Sqs\Exception\SqsException;
use Aws\Sqs\SqsClient;
use NewImmoGroup\AwsBroker\Entity\ApproximateNumberOfMessageInQueue;
use NewImmoGroup\AwsBroker\Entity\Configuration;
use NewImmoGroup\AwsBroker\Exception\InvalidParameterException;
use NewImmoGroup\AwsBroker\Exception\MissingParameterException;
use Psr\Log\LoggerInterface;

/**
 * @phpstan-import-type Options from Configuration
 */
class SqsAdapter
{
    private SqsClient $client;
    private Configuration $configuration;
    private LoggerInterface $logger;
    private CacheInterface $cache;

    public function __construct(
        Configuration $configuration,
        LoggerInterface $logger
    ) {
        $this->configuration = $configuration;
        $this->logger = $logger;
        $this->cache = new ArrayCache();
    }

    private function getClient(): SqsClient
    {
        return $this->client ??= self::createClientFromEnvAndDefaultProvider(
            $this->configuration->getOptions(),
            $this->cache
        );
    }

    /**
     * @param Options $options
     */
    public static function createClientFromEnvAndDefaultProvider(
        array $options,
        CacheInterface $cache
    ): SqsClient {
        return new SqsClient(
            [
                'credentials' => CredentialProvider::defaultProvider(['credentials' => $cache]),
                'region' => getenv('AWS_REGION'),
                'version' => getenv('AWS_VERSION'),
                'http' => [
                    'connect_timeout' => 5,
                    'timeout' => 21, // max wait time is 20 seconds
                ],
            ] + $options
        );
    }

    /**
     * @param array{
     *     QueueName?: string,
     *     tags?: array<string, string>,
     *     Attributes?: array{
     *          DelaySeconds?: int,
     *          MessageRetentionPeriod?: int
     *     }
     * } $args
     *
     * @throws MissingParameterException when queue name is not defined
     * @throws SqsException              when queue failed to be created
     * @throws InvalidParameterException
     */
    public function createQueue(
        array $args = []
    ): void {
        $this->configuration->disableAutoSetup();
        $args['QueueName'] ??= $this->configuration->getQueueName();
        $args['tags'] ??= $this->configuration->getQueueTags();
        $this->logger->debug('Preparing to create queue', $args);
        if ($this->queueExists($args['QueueName'])) {
            $this->logger->debug('Queue already exists, skipping', $args);

            return;
        }
        $this->logger->debug('Creating queue', $args);

        $this->getClient()->createQueue($args);

        /*
         * we must wait at least one second after the queue
         * is created to be able to use it.
         */
        sleep(1);
    }

    /**
     * @param array{
     *     MessageBody: string,
     *     QueueUrl?: string,
     *     DelaySeconds?: int,
     *     MessageDeduplicationId?: string,
     *     MessageGroupId?: string
     * } $args
     *
     * @throws MissingParameterException when DSN is missing the account ID
     * @throws InvalidParameterException when DSN is not valid
     * @throws SqsException              when message failed to sent
     */
    public function sendMessage(
        array $args
    ): void {
        $args['QueueUrl'] ??= $this->configuration->getQueueUrl();

        $this->getClient()->sendMessage($args);
    }

    /**
     * @param array{
     *     QueueUrl?: string,
     *     MaxNumberOfMessages?: int,
     *     WaitTimeSeconds?: int,
     *     VisibilityTimeout?: int,
     *     AttributeNames?: string[],
     *     MessageAttributeNames?: string[],
     *     ReceiveRequestAttemptId?: string[],
     * } $args
     *
     * @return array<array{Body: string, ReceiptHandle: string}>
     *
     * @throws MissingParameterException when DSN is missing the account ID
     * @throws InvalidParameterException when DSN is not valid
     * @throws SqsException              when messages failed to be fetched
     */
    public function receiveMessage(
        array $args = []
    ): array {
        $args['QueueUrl'] ??= $this->configuration->getQueueUrl();
        $args['MaxNumberOfMessages'] ??= $this->configuration->getMaxMessage();
        $args['WaitTimeSeconds'] ??= $this->configuration->getWaitTime();
        $args['VisibilityTimeout'] ??= $this->configuration->getVisibilityTimeout();

        /* @phpstan-ignore-next-line */
        return $this
            ->getClient()
            ->receiveMessage($args)['Messages'] ?? [];
    }

    /**
     * @throws SqsException when message failed to be deleted
     */
    public function deleteMessage(
        string $queueUrl,
        string $receiptHandle
    ): void {
        $this->getClient()->deleteMessage(
            [
                'QueueUrl' => $queueUrl,
                'ReceiptHandle' => $receiptHandle,
            ]
        );
    }

    public function hasAutoSetup(): bool
    {
        return $this->configuration->hasAutoSetup();
    }

    /**
     * @throws MissingParameterException when DSN is missing the account ID
     * @throws InvalidParameterException when DSN is not valid
     */
    public function getQueueUrl(): string
    {
        return $this->configuration->getQueueUrl();
    }

    /**
     * @throws MissingParameterException when DSN is missing the account ID
     * @throws InvalidParameterException when DSN is not valid
     */
    public function getQueueUrlForName(string $queueName): string
    {
        return $this->configuration->queueUrlForName($queueName);
    }

    /**
     * @throws MissingParameterException
     */
    public function getQueueName(): string
    {
        return $this->configuration->getQueueName();
    }

    public function getApproximateNumberOfMessages(): ApproximateNumberOfMessageInQueue
    {
        /**
         * @var array{
         * ApproximateNumberOfMessages: string,
         * ApproximateNumberOfMessagesDelayed: string,
         * ApproximateNumberOfMessagesNotVisible: string,
         * } $vars
         */
        $vars = $this->getClient()->getQueueAttributes([
            'QueueUrl' => $this->configuration->getQueueUrl(),
            'AttributeNames' => [
                'ApproximateNumberOfMessages',
                'ApproximateNumberOfMessagesDelayed',
                'ApproximateNumberOfMessagesNotVisible',
            ],
        ])['Attributes'];

        return new ApproximateNumberOfMessageInQueue(
            (int) $vars['ApproximateNumberOfMessages'],
            (int) $vars['ApproximateNumberOfMessagesDelayed'],
            (int) $vars['ApproximateNumberOfMessagesNotVisible'],
        );
    }

    public function autoSetup(): void
    {
        if ($this->hasAutoSetup()) {
            $this->createQueue();
        }
    }

    /**
     * @throws InvalidParameterException
     * @throws MissingParameterException
     */
    private function queueExists(string $queueName): bool
    {
        try {
            $this->getClient()->getQueueUrl([
                'QueueName' => $queueName,
                'QueueOwnerAWSAccountId' => $this->configuration->findAccountIdFromDsn(),
            ]);

            return true;
        } catch (SqsException $e) {
            if (SqsTransport::isQueueNotFoundException($e)) {
                return false;
            }
            throw $e;
        }
    }
}
