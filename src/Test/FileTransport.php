<?php

declare(strict_types=1);

namespace NewImmoGroup\AwsBroker\Test;

use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Transport\Serialization\SerializerInterface;
use Symfony\Component\Messenger\Transport\TransportInterface;

final class FileTransport implements TransportInterface
{
    private string $filename;
    private SerializerInterface $serializer;

    public function __construct(string $filename, SerializerInterface $serializer)
    {
        $this->filename = $filename;
        $this->serializer = $serializer;
    }

    public function get(): iterable
    {
        $content = file_get_contents($this->filename);
        assert(false !== $content);
        $decoded = json_decode($content, true, 512, JSON_THROW_ON_ERROR);
        $encodedEnvelope = [
            /* @phpstan-ignore-next-line We expect to fail if we can't parse */
            'body' => json_encode($decoded['body'], JSON_THROW_ON_ERROR),
            /* @phpstan-ignore-next-line We expect to fail if we can't parse */
            'headers' => $decoded['headers'],
        ];

        return [$this->serializer->decode($encodedEnvelope)];
    }

    public function ack(Envelope $envelope): void
    {
        // stub, this isn't used yet
    }

    public function reject(Envelope $envelope): void
    {
        // stub, this isn't used yet
    }

    public function send(Envelope $envelope): Envelope
    {
        $encodedEnvelope = $this->serializer->encode($envelope);

        $info = [
            'body' => json_decode($encodedEnvelope['body']),
            'headers' => $encodedEnvelope['headers'],
        ];

        file_put_contents($this->filename, json_encode($info, JSON_PRETTY_PRINT | JSON_THROW_ON_ERROR));

        return $envelope;
    }
}
