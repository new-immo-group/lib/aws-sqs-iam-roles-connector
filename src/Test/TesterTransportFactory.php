<?php

declare(strict_types=1);

namespace NewImmoGroup\AwsBroker\Test;

use NewImmoGroup\AwsBroker\Entity\Configuration;
use Symfony\Component\Messenger\Transport\Serialization\SerializerInterface;
use Symfony\Component\Messenger\Transport\TransportFactoryInterface;
use Symfony\Component\Messenger\Transport\TransportInterface;

/**
 * @phpstan-import-type Options from Configuration
 */
class TesterTransportFactory implements TransportFactoryInterface
{
    private const DSN_STARTS_WITH = 'file://';

    /**
     * @param array<string, mixed> $options is not used
     */
    public function createTransport(
        string $dsn,
        array $options,
        SerializerInterface $serializer
    ): TransportInterface {
        return new FileTransport(
            substr($dsn, strlen(self::DSN_STARTS_WITH)),
            $serializer,
        );
    }

    /**
     * @param array<string, mixed> $options is not used
     */
    public function supports(
        string $dsn,
        array $options
    ): bool {
        return 0 === strpos($dsn, self::DSN_STARTS_WITH);
    }
}
