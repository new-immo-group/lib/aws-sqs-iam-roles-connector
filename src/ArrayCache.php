<?php

declare(strict_types=1);

namespace NewImmoGroup\AwsBroker;

use Aws\CacheInterface;

final class ArrayCache implements CacheInterface
{
    /**
     * @var array<string, mixed>
     */
    private array $cache = [];

    /**
     * @param string $key
     *
     * @return mixed
     */
    public function get($key)
    {
        return $this->cache[$key] ?? null;
    }

    /**
     * @param string $key
     * @param mixed  $value
     * @param int    $ttl
     */
    public function set($key, $value, $ttl = 0): void
    {
        $this->cache[$key] = $value;
    }

    /** @param string $key */
    public function remove($key): void
    {
        unset($this->cache[$key]);
    }
}
