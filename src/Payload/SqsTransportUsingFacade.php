<?php

declare(strict_types=1);

namespace NewImmoGroup\AwsBroker\Payload;

use LogicException;
use NewImmoGroup\AwsBroker\Entity\AddressToQueueStamp;
use NewImmoGroup\AwsBroker\Entity\Stamp;
use NewImmoGroup\AwsBroker\Event\EventDispatcherInterface;
use NewImmoGroup\AwsBroker\Payload\ReceiveMessage\Response;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Exception\MessageDecodingFailedException;
use Symfony\Component\Messenger\Exception\TransportException;
use Symfony\Component\Messenger\Stamp\DelayStamp;
use Symfony\Component\Messenger\Stamp\NonSendableStampInterface;
use Symfony\Component\Messenger\Stamp\StampInterface;
use Symfony\Component\Messenger\Transport\Serialization\SerializerInterface;
use Symfony\Component\Messenger\Transport\SetupableTransportInterface;
use Symfony\Component\Messenger\Transport\TransportInterface;
use Throwable;

final class SqsTransportUsingFacade implements TransportInterface, SetupableTransportInterface
{
    private const REMOVED_STAMPS = [
        NonSendableStampInterface::class,
        DelayStamp::class,
    ];
    private SqsFacadeInterface $facade;
    private SerializerInterface $serializer;
    private EventDispatcherInterface $eventDispatcher;
    private SqsTransportOption $config;
    private bool $hasSetUp = false;

    public function __construct(
        SqsFacadeInterface $facade,
        SerializerInterface $serializer,
        EventDispatcherInterface $eventDispatcher,
        SqsTransportOption $config
    ) {
        $this->facade = $facade;
        $this->serializer = $serializer;
        $this->eventDispatcher = $eventDispatcher;
        $this->config = $config;
    }

    public function get(): iterable
    {
        $this->autoSetup();
        $generator = $this->facade->requestReceiveMessage();
        foreach ($generator as $response) {
            yield $this->responseToEnvelope($response);
        }
    }

    public function ack(Envelope $envelope): void
    {
        $stamp = $this->getLastStamp($envelope);
        $this->facade->requestDeleteMessage($stamp->getReceiptHandle());
    }

    public function reject(Envelope $envelope): void
    {
        $stamp = $this->getLastStamp($envelope);
        $this->facade->requestDeleteMessage($stamp->getReceiptHandle());
    }

    public function send(Envelope $envelope): Envelope
    {
        $this->autoSetup();

        $replyToStamp = $envelope->last(AddressToQueueStamp::class);
        $delayStamp = $envelope->last(DelayStamp::class);

        $delay = null;

        if ($delayStamp instanceof DelayStamp) {
            $delayInMs = $delayStamp->getDelay();
            $delay = ((int) floor($delayInMs / 1000));
        }

        $envelope = self::editStamps($envelope);

        $body = $this->encode($envelope);

        if ($replyToStamp instanceof AddressToQueueStamp) {
            $this->facade->requestSendMessageForceQueueUrl(
                $body,
                $delay,
                $replyToStamp->queueName
            );
        } else {
            $this->facade->requestSendMessage($body, $delay);
        }

        return $envelope;
    }

    private function encode(
        Envelope $envelope
    ): string {
        try {
            return json_encode(
                $this->serializer->encode($envelope),
                JSON_THROW_ON_ERROR
            );
        } catch (Throwable $exception) {
            throw new TransportException(
                'Failed to encode Envelope',
                $exception->getCode(),
                $exception
            );
        }
    }

    public function setup(): void
    {
        if ($this->hasSetUp) {
            return;
        }
        $this->hasSetUp = true;
        if ($this->config->forceConfigurationReinitialisation) {
            $this->facade->idempotentQueueCreation(
                $this->config->tags,
                $this->config->attributeDict
            );
        } else {
            $this->facade->createDefaultQueueIfNotExists();
        }
    }

    private function responseToEnvelope(
        Response $response
    ): Envelope {
        return $this
            ->decode($response->body)
            ->with(
                new Stamp(
                    $this->facade->getQueueUrl(),
                    $response->receiptHandle
                )
            );
    }

    private function decode(
        string $messageBody
    ): Envelope {
        try {
            $message = json_decode(
                $messageBody,
                true,
                512,
                JSON_THROW_ON_ERROR
            );

            if (!is_array($message)) {
                throw new LogicException('Invalid message body, array expected');
            }

            $this->eventDispatcher->messageDecoded($message);

            return $this->serializer->decode($message);
        } catch (Throwable $exception) {
            throw new MessageDecodingFailedException(
                'Failed to decode Envelope',
                $exception->getCode(),
                $exception
            );
        }
    }

    private function getLastStamp(Envelope $envelope): Stamp
    {
        $stamp = $envelope->last(Stamp::class);
        if (!$stamp instanceof Stamp) {
            throw new LogicException('Envelope has no Stamp');
        }

        return $stamp;
    }

    public function autoSetup(): void
    {
        if ($this->config->autoSetup) {
            $this->config->autoSetup = false;
            $this->setup();
        }
    }

    /**
     * this function exists because Envelope::withoutAll clones everything,
     * and we cannot provide multiple classes to remove.
     *
     * So using Envelope::withoutAll multiple times to exclude multiple classes is heavy
     */
    private static function editStamps(Envelope $envelope): Envelope
    {
        $stamps = $envelope->all();
        $newStamps = [];

        array_walk_recursive($stamps, static function (StampInterface $stamp) use (&$newStamps) {
            foreach (self::REMOVED_STAMPS as $removedStampClass) {
                if ($stamp instanceof $removedStampClass) {
                    return;
                }
            }
            $newStamps[] = $stamp;
        });

        return new Envelope($envelope->getMessage(), $newStamps);
    }
}
