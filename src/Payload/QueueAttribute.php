<?php

declare(strict_types=1);

namespace NewImmoGroup\AwsBroker\Payload;

final class QueueAttribute
{
    public const APPROXIMATE_NUMBER_OF_MESSAGES = 'ApproximateNumberOfMessages';
    public const APPROXIMATE_NUMBER_OF_MESSAGES_DELAYED = 'ApproximateNumberOfMessagesDelayed';
    public const APPROXIMATE_NUMBER_OF_MESSAGES_NOT_VISIBLE = 'ApproximateNumberOfMessagesNotVisible';
    public const CREATED_TIMESTAMP = 'CreatedTimestamp';
    public const DELAY_SECONDS = 'DelaySeconds';
    public const LAST_MODIFIED_TIMESTAMP = 'LastModifiedTimestamp';
    public const MAXIMUM_MESSAGE_SIZE = 'MaximumMessageSize';
    public const MESSAGE_RETENTION_PERIOD = 'MessageRetentionPeriod';
    public const POLICY = 'Policy';
    public const REDRIVE_POLICY = 'RedrivePolicy';
    public const QUEUE_ARN = 'QueueArn';
    public const RECEIVE_MESSAGE_WAIT_TIME_SECONDS = 'ReceiveMessageWaitTimeSeconds';
    public const VISIBILITY_TIMEOUT = 'VisibilityTimeout';

    public const CASES = [
        self::APPROXIMATE_NUMBER_OF_MESSAGES,
        self::APPROXIMATE_NUMBER_OF_MESSAGES_DELAYED,
        self::APPROXIMATE_NUMBER_OF_MESSAGES_NOT_VISIBLE,
        self::CREATED_TIMESTAMP,
        self::DELAY_SECONDS,
        self::LAST_MODIFIED_TIMESTAMP,
        self::MAXIMUM_MESSAGE_SIZE,
        self::MESSAGE_RETENTION_PERIOD,
        self::POLICY,
        self::REDRIVE_POLICY,
        self::QUEUE_ARN,
        self::RECEIVE_MESSAGE_WAIT_TIME_SECONDS,
        self::VISIBILITY_TIMEOUT,
    ];
}
