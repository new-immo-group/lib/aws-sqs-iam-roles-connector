<?php

declare(strict_types=1);

namespace NewImmoGroup\AwsBroker\Payload;

use Generator;

interface SqsFacadeInterface
{
    /**
     * @param array<string, string> $tags
     */
    public function requestCreateQueue(array $tags, AttributeDict $dict): CreateQueue\Response;

    /** @param list<string> $attributes */
    public function requestGetQueueAttributes(array $attributes): GetQueueAttributes\Response;

    /**
     * returns null if queue does not exist.
     */
    public function requestListQueueTagsIfQueueExists(): ?ListQueueTags\Response;

    public function requestSetQueueAttributes(AttributeDict $attributes): SetQueueAttributes\Response;

    public function requestDeleteQueue(): DeleteQueue\Response;

    public function requestListQueueTags(): ListQueueTags\Response;

    /**
     * @param array<string, string> $tags
     */
    public function requestTagQueue(array $tags): TagQueue\Response;

    /** @param list<string> $tagKeys */
    public function requestUntagQueue(array $tagKeys): UntagQueue\Response;

    /**
     * @return Generator<ReceiveMessage\Response>
     */
    public function requestReceiveMessage(): Generator;

    public function requestSendMessage(string $body, ?int $delay): SendMessage\Response;

    public function requestSendMessageForceQueueUrl(string $body, ?int $delay, string $queueUrl): SendMessage\Response;

    public function requestDeleteMessage(string $receiptHandle): DeleteMessage\Response;

    public function createDefaultQueueIfNotExists(): void;

    /** @param array<string, string> $tags */
    public function idempotentQueueCreation(array $tags, AttributeDict $dict): void;

    public function getQueueUrl(): string;
}
