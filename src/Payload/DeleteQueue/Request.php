<?php

declare(strict_types=1);

namespace NewImmoGroup\AwsBroker\Payload\DeleteQueue;

use Aws\Sqs\SqsClient;
use NewImmoGroup\AwsBroker\Payload\ApiRequest;
use NewImmoGroup\AwsBroker\Payload\AttributeDict;

/**
 * @see https://docs.aws.amazon.com/AWSSimpleQueueService/latest/APIReference/API_DeleteQueue.html
 *
 * @phpstan-import-type AttributeDictPayload from AttributeDict
 *
 * @implements ApiRequest<Response>
 */
final class Request implements ApiRequest
{
    private string $queueUrl;

    public function __construct(string $queueUrl)
    {
        $this->queueUrl = $queueUrl;
    }

    /**
     * @return array{
     *     QueueUrl: string,
     * }
     */
    public function asPayload(): array
    {
        return ['QueueUrl' => $this->queueUrl];
    }

    public function accept(SqsClient $client): Response
    {
        $client->deleteQueue($this->asPayload());

        // This payload is empty
        // I think it is ok to make an empty payload in response
        return new Response();
    }
}
