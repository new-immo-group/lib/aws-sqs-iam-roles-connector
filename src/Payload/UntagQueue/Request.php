<?php

declare(strict_types=1);

namespace NewImmoGroup\AwsBroker\Payload\UntagQueue;

use Aws\Sqs\SqsClient;
use NewImmoGroup\AwsBroker\Payload\ApiRequest;

/**
 * @see https://docs.aws.amazon.com/AWSSimpleQueueService/latest/APIReference/API_UntagQueue.html
 *
 * @implements ApiRequest<Response>
 */
final class Request implements ApiRequest
{
    public string $queueUrl;
    /** @var list<string> */
    public array $tagKeys;

    /** @param list<string> $tagKeys */
    public function __construct(string $queueUrl, array $tagKeys)
    {
        $this->queueUrl = $queueUrl;
        $this->tagKeys = $tagKeys;
    }

    /**
     * @return array{QueueUrl: string}
     */
    public function asPayload(): array
    {
        return [
            'QueueUrl' => $this->queueUrl,
            'TagKeys' => $this->tagKeys,
        ];
    }

    public function accept(SqsClient $client): Response
    {
        $client->untagQueue($this->asPayload());

        return new Response();
    }
}
