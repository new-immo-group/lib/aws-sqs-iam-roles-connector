<?php

declare(strict_types=1);

namespace NewImmoGroup\AwsBroker\Payload\SetQueueAttributes;

use Aws\Sqs\SqsClient;
use NewImmoGroup\AwsBroker\Payload\ApiRequest;
use NewImmoGroup\AwsBroker\Payload\AttributeDict;

/**
 * @see https://docs.aws.amazon.com/AWSSimpleQueueService/latest/APIReference/API_SetQueueAttributes.html
 *
 * @phpstan-import-type AttributeDictPayload from AttributeDict
 *
 * @implements ApiRequest<Response>
 */
final class Request implements ApiRequest
{
    public string $queueUrl;
    public AttributeDict $attributes;

    public function __construct(string $queueUrl, AttributeDict $attributes)
    {
        $this->queueUrl = $queueUrl;
        $this->attributes = $attributes;
    }

    /**
     * @return array{
     *     QueueUrl: string,
     *     Attributes: AttributeDictPayload,
     * }
     */
    public function asPayload(): array
    {
        return [
            'QueueUrl' => $this->queueUrl,
            'Attributes' => $this->attributes->asPayload(),
        ];
    }

    public function accept(SqsClient $client): Response
    {
        $client->setQueueAttributes($this->asPayload());

        return new Response();
    }
}
