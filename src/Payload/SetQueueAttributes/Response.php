<?php

declare(strict_types=1);

namespace NewImmoGroup\AwsBroker\Payload\SetQueueAttributes;

use NewImmoGroup\AwsBroker\Payload\ApiResponse;

final class Response implements ApiResponse
{
}
