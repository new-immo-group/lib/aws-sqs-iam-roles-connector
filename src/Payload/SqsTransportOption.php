<?php

declare(strict_types=1);

namespace NewImmoGroup\AwsBroker\Payload;

final class SqsTransportOption
{
    public string $queueName;
    public bool $autoSetup;
    public bool $forceConfigurationReinitialisation;
    /** @var array<string, mixed> */
    public array $awsClientOptions;
    /** @var array<string, string> */
    public array $tags;
    public AttributeDict $attributeDict;

    /**
     * @param array{
     *     queue_prefix?: string,
     *     queue_name: string,
     *     auto_setup?: bool,
     *     aws_client_options?: array<string, mixed>,
     *     queue_tags?: array<string, string>,
     *     queue_creation?: array{
     *         tags?: array<string, string>,
     *         force_configuration_reinitialisation?: bool,
     *         delay_seconds?: int,
     *         message_retention_period?: int,
     *         receive_message_wait_time_seconds?: int,
     *         visibility_timeout?: int,
     *     }
     * } $transportOption
     */
    public static function from(array $transportOption): self
    {
        $self = new self();
        $self->queueName = ($transportOption['queue_prefix'] ?? '').$transportOption['queue_name'];
        $self->autoSetup = (bool) ($transportOption['auto_setup'] ?? false);
        $self->awsClientOptions = $transportOption['aws_client_options'] ?? [];

        $queueCreation = $transportOption['queue_creation'] ?? [];
        $queueCreation['tags'] = $queueCreation['tags'] ?? [];

        $self->tags = array_merge(
            $transportOption['queue_tags'] ?? [],
            $queueCreation['tags'],
        );
        $self->attributeDict = new AttributeDict();

        $self->forceConfigurationReinitialisation = (bool) ($queueCreation['force_configuration_reinitialisation'] ?? false);

        if (isset($queueCreation['delay_seconds']) && $delay = (int) $queueCreation['delay_seconds']) {
            $self->attributeDict->DelaySeconds = "$delay";
        }

        if (isset($queueCreation['message_retention_period']) && $messageRetentionPeriod = (int) $queueCreation['message_retention_period']) {
            $self->attributeDict->MessageRetentionPeriod = "$messageRetentionPeriod";
        }

        if (isset($queueCreation['receive_message_wait_time_seconds']) && $receiveMessageWaitTimeSeconds = (int) $queueCreation['receive_message_wait_time_seconds']) {
            $self->attributeDict->ReceiveMessageWaitTimeSeconds = "$receiveMessageWaitTimeSeconds";
        }

        if (isset($queueCreation['visibility_timeout']) && $visibilityTimeout = (int) $queueCreation['visibility_timeout']) {
            $self->attributeDict->VisibilityTimeout = "$visibilityTimeout";
        }

        return $self;
    }
}
