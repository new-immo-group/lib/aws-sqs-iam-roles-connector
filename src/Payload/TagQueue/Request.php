<?php

declare(strict_types=1);

namespace NewImmoGroup\AwsBroker\Payload\TagQueue;

use Aws\Sqs\SqsClient;
use NewImmoGroup\AwsBroker\Payload\ApiRequest;

/**
 * @see https://docs.aws.amazon.com/AWSSimpleQueueService/latest/APIReference/API_TagQueue.html
 *
 * @implements ApiRequest<Response>
 */
final class Request implements ApiRequest
{
    public string $queueUrl;
    /** @var array<string, string> */
    public array $tags;

    /** @param array<string, string> $tags */
    public function __construct(string $queueUrl, array $tags)
    {
        $this->queueUrl = $queueUrl;
        $this->tags = $tags;
    }

    /**
     * @return array{QueueUrl: string}
     */
    public function asPayload(): array
    {
        return [
            'QueueUrl' => $this->queueUrl,
            'Tags' => $this->tags,
        ];
    }

    public function accept(SqsClient $client): Response
    {
        $client->tagQueue($this->asPayload());

        return new Response();
    }
}
