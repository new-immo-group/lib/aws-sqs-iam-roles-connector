<?php

declare(strict_types=1);

namespace NewImmoGroup\AwsBroker\Payload;

/**
 * @phpstan-type AttributeDictPayload array{
 *  ApproximateNumberOfMessages?: string,
 *  ApproximateNumberOfMessagesDelayed?: string,
 *  ApproximateNumberOfMessagesNotVisible?: string,
 *  CreatedTimestamp?: string,
 *  DelaySeconds?: string,
 *  LastModifiedTimestamp?: string,
 *  MaximumMessageSize?: string,
 *  MessageRetentionPeriod?: string,
 *  Policy?: string,
 *  RedrivePolicy?: string,
 *  QueueArn?: string,
 *  ReceiveMessageWaitTimeSeconds?: string,
 *  VisibilityTimeout?: string,
 *  }
 */
final class AttributeDict
{
    public string $ApproximateNumberOfMessages;
    public string $ApproximateNumberOfMessagesDelayed;
    public string $ApproximateNumberOfMessagesNotVisible;
    public string $CreatedTimestamp;
    public string $DelaySeconds;
    public string $LastModifiedTimestamp;
    public string $MaximumMessageSize;
    public string $MessageRetentionPeriod;
    public string $Policy;
    public string $RedrivePolicy;
    public string $QueueArn;
    public string $ReceiveMessageWaitTimeSeconds;
    public string $VisibilityTimeout;

    /**
     * @return AttributeDictPayload
     */
    public function asPayload(): array
    {
        $attributes = [];
        foreach (QueueAttribute::CASES as $case) {
            if (isset($this->$case)) {
                $attributes[$case] = $this->$case;
            }
        }

        return $attributes;
    }
}
