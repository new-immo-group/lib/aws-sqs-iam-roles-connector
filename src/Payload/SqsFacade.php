<?php

declare(strict_types=1);

namespace NewImmoGroup\AwsBroker\Payload;

use Aws\CacheInterface;
use Aws\Credentials\CredentialProvider;
use Aws\Sqs\Exception\SqsException;
use Aws\Sqs\SqsClient;
use Generator;
use NewImmoGroup\AwsBroker\Payload\CreateQueue\Request as CreateQueueRequest;
use NewImmoGroup\AwsBroker\Payload\DeleteMessage\Request as DeleteMessageRequest;
use NewImmoGroup\AwsBroker\Payload\DeleteQueue\Request as DeleteQueueRequest;
use NewImmoGroup\AwsBroker\Payload\GetQueueAttributes\Request as GetQueueAttributesRequest;
use NewImmoGroup\AwsBroker\Payload\ListQueueTags\Request as ListQueueTagsRequest;
use NewImmoGroup\AwsBroker\Payload\ReceiveMessage\Request as ReceiveMessageRequest;
use NewImmoGroup\AwsBroker\Payload\SendMessage\Request as SendMessageRequest;
use NewImmoGroup\AwsBroker\Payload\SetQueueAttributes\Request as SetQueueAttributesRequest;
use NewImmoGroup\AwsBroker\Payload\TagQueue\Request as TagQueueRequest;
use NewImmoGroup\AwsBroker\Payload\UntagQueue\Request as UntagQueueRequest;
use RuntimeException;

final class SqsFacade implements SqsFacadeInterface
{
    private string $region;
    private string $version;
    public string $accountId;
    public SqsClient $client;
    private string $queueName;
    public string $queueUrl;

    /**
     * @param array<string, mixed> $awsClientOptions
     */
    public function __construct(
        string $region,
        string $version,
        string $accountId,
        array $awsClientOptions,
        CacheInterface $cache,
        string $queueName,
        string $queueUrl
    ) {
        $this->region = $region;
        $this->version = $version;
        $this->accountId = $accountId;
        $this->client = new SqsClient(
            array_merge([
                'credentials' => CredentialProvider::defaultProvider(['credentials' => $cache]),
                'region' => $this->region,
                'version' => $this->version,
                'http' => [
                    'connect_timeout' => 5,
                    'timeout' => 21, // max wait time is 20 seconds
                ],
            ], $awsClientOptions)
        );
        $this->queueName = $queueName;
        $this->queueUrl = $queueUrl;
    }

    public static function getEnvAsString(string $str): string
    {
        $env = getenv($str);
        if (!is_string($env)) {
            $envAsString = print_r($env, true);
            throw new RuntimeException("Unknown environment variable $str with value '$envAsString'");
        }

        return $env;
    }

    public static function makeQueueUrl(string $region, string $account, string $queueName): string
    {
        return "https://sqs.$region.amazonaws.com/$account/$queueName";
    }

    public static function exceptionIsQueueDoesNotExist(SqsException $exception): bool
    {
        return 'AWS.SimpleQueueService.NonExistentQueue' === (string) $exception->getAwsErrorCode();
    }

    public function requestCreateQueue(array $tags, AttributeDict $dict): CreateQueue\Response
    {
        return (new CreateQueueRequest(
            $this->queueName,
            $tags,
            $dict
        )
        )
            ->accept($this->client);
    }

    public function requestGetQueueAttributes(array $attributes): GetQueueAttributes\Response
    {
        return (new GetQueueAttributesRequest($this->queueUrl, $attributes))->accept($this->client);
    }

    public function requestListQueueTagsIfQueueExists(): ?ListQueueTags\Response
    {
        try {
            return $this->requestListQueueTags();
        } catch (SqsException $exception) {
            if (self::exceptionIsQueueDoesNotExist($exception)) {
                return null;
            }
            throw $exception;
        }
    }

    public function requestSetQueueAttributes(AttributeDict $attributes): SetQueueAttributes\Response
    {
        return (new SetQueueAttributesRequest($this->queueUrl, $attributes))->accept($this->client);
    }

    public function requestDeleteQueue(): DeleteQueue\Response
    {
        return (new DeleteQueueRequest($this->queueUrl))->accept($this->client);
    }

    public function requestListQueueTags(): ListQueueTags\Response
    {
        return (new ListQueueTagsRequest($this->queueUrl))->accept($this->client);
    }

    public function requestTagQueue(array $tags): TagQueue\Response
    {
        return (new TagQueueRequest($this->queueUrl, $tags))->accept($this->client);
    }

    public function requestUntagQueue(array $tagKeys): UntagQueue\Response
    {
        return (new UntagQueueRequest($this->queueUrl, $tagKeys))->accept($this->client);
    }

    public function requestReceiveMessage(): Generator
    {
        return (new ReceiveMessageRequest(
            $this->queueUrl,
            null,
            null,
            null,
        ))->accept($this->client);
    }

    public function requestSendMessage(string $body, ?int $delay): SendMessage\Response
    {
        return $this->requestSendMessageForceQueueUrl($body, $delay, $this->queueUrl);
    }

    public function requestSendMessageForceQueueUrl(string $body, ?int $delay, string $queueUrl): SendMessage\Response
    {
        return (new SendMessageRequest(
            $queueUrl,
            $body,
            null,
        ))->accept($this->client);
    }

    public function requestDeleteMessage(string $receiptHandle): DeleteMessage\Response
    {
        return (new DeleteMessageRequest($this->queueUrl, $receiptHandle))->accept($this->client);
    }

    public function createDefaultQueueIfNotExists(): void
    {
        if (null === $this->requestListQueueTagsIfQueueExists()) {
            $this->requestCreateQueue([], new AttributeDict());
        }
    }

    public function idempotentQueueCreation(array $tags, AttributeDict $dict): void
    {
        $values = $this->requestListQueueTagsIfQueueExists();

        if (null === $values) {
            $this->requestCreateQueue($tags, $dict);

            return;
        }
        $existingTags = $values->tags;
        $addTag = [];
        foreach ($tags as $key => $tag) {
            if (isset($existingTags[$key])) {
                unset($existingTags[$key]);
            } else {
                $addTag[$key] = $tag;
            }
        }

        $untag = array_keys($existingTags);

        $this->requestSetQueueAttributes($dict);
        $this->requestTagQueue($addTag);
        $this->requestUntagQueue($untag);
    }

    public function getQueueUrl(): string
    {
        return $this->queueUrl;
    }
}
