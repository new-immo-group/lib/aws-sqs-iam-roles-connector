<?php

declare(strict_types=1);

namespace NewImmoGroup\AwsBroker\Payload\SendMessage;

use Aws\Sqs\SqsClient;
use NewImmoGroup\AwsBroker\Payload\ApiRequest;
use RuntimeException;

/**
 * @see https://docs.aws.amazon.com/AWSSimpleQueueService/latest/APIReference/API_SendMessage.html
 *
 * @implements ApiRequest<Response>
 */
final class Request implements ApiRequest
{
    public string $queueUrl;
    public string $messageBody;
    public ?int $delaySeconds;

    public function __construct(string $queueUrl, string $messageBody, ?int $delaySeconds)
    {
        $this->queueUrl = $queueUrl;
        $this->messageBody = $messageBody;
        $this->delaySeconds = $delaySeconds;
    }

    /**
     * @return array{
     *     DelaySeconds?: int,
     *     MessageBody: string,
     *     QueueUrl: string,
     * }
     */
    public function asPayload(): array
    {
        return array_filter([
            'DelaySeconds' => $this->delaySeconds,
            'MessageBody' => $this->messageBody,
            'QueueUrl' => $this->queueUrl,
        ], fn ($value) => null !== $value);
    }

    public function accept(SqsClient $client): Response
    {
        $attributes = $client->sendMessage($this->asPayload());
        $MD5OfMessageBody = $attributes['MD5OfMessageBody'];

        if ($MD5OfMessageBody !== md5($this->messageBody)) {
            throw new RuntimeException('bad md5 for message body');
        }
        $messageId = $attributes['MessageId'];

        assert(is_string($MD5OfMessageBody));
        assert(is_string($messageId));

        return new Response(
            $messageId,
        );
    }
}
