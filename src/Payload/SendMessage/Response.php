<?php

declare(strict_types=1);

namespace NewImmoGroup\AwsBroker\Payload\SendMessage;

use NewImmoGroup\AwsBroker\Payload\ApiResponse;

final class Response implements ApiResponse
{
    public string $messageId;

    public function __construct(string $messageId)
    {
        $this->messageId = $messageId;
    }
}
