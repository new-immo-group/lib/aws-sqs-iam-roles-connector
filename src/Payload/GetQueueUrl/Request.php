<?php

declare(strict_types=1);

namespace NewImmoGroup\AwsBroker\Payload\GetQueueUrl;

use Aws\Sqs\SqsClient;
use NewImmoGroup\AwsBroker\Payload\ApiRequest;

/**
 * @see https://docs.aws.amazon.com/AWSSimpleQueueService/latest/APIReference/API_GetQueueUrl.html
 *
 * @implements ApiRequest<Response>
 */
final class Request implements ApiRequest
{
    public string $queueName;
    public string $queueOwnerAWSAccountId;

    public function __construct(string $queueName, string $queueOwnerAWSAccountId)
    {
        $this->queueName = $queueName;
        $this->queueOwnerAWSAccountId = $queueOwnerAWSAccountId;
    }

    /**
     * @return array{
     *     QueueName: string,
     *     QueueOwnerAWSAccountId: string,
     * }
     */
    public function asPayload(): array
    {
        return [
            'QueueName' => $this->queueName,
            'QueueOwnerAWSAccountId' => $this->queueOwnerAWSAccountId,
        ];
    }

    public function accept(SqsClient $client): Response
    {
        $payload = $client->getQueueUrl($this->asPayload());
        $queueUrl = $payload['QueueUrl'];
        assert(is_string($queueUrl));

        return new Response($queueUrl);
    }
}
