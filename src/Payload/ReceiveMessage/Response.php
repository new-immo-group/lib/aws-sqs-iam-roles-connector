<?php

declare(strict_types=1);

namespace NewImmoGroup\AwsBroker\Payload\ReceiveMessage;

use NewImmoGroup\AwsBroker\Payload\ApiResponse;

final class Response implements ApiResponse
{
    public string $messageId;
    public string $body;
    public string $receiptHandle;

    public function __construct(string $messageId, string $body, string $receiptHandle)
    {
        $this->messageId = $messageId;
        $this->body = $body;
        $this->receiptHandle = $receiptHandle;
    }
}
