<?php

declare(strict_types=1);

namespace NewImmoGroup\AwsBroker\Payload\ReceiveMessage;

use Aws\Sqs\SqsClient;
use Generator;
use NewImmoGroup\AwsBroker\Payload\ApiRequestIterable;
use RuntimeException;

/**
 * @see https://docs.aws.amazon.com/AWSSimpleQueueService/latest/APIReference/API_ReceiveMessage.html
 *
 * @implements ApiRequestIterable<Response>
 */
final class Request implements ApiRequestIterable
{
    public string $queueUrl;
    public ?int $maxNumberOfMessages;
    public ?int $visibilityTimeout;
    public ?int $waitTimeSeconds;

    public function __construct(string $queueUrl, ?int $maxNumberOfMessages, ?int $visibilityTimeout, ?int $waitTimeSeconds)
    {
        $this->queueUrl = $queueUrl;
        $this->maxNumberOfMessages = $maxNumberOfMessages;
        $this->visibilityTimeout = $visibilityTimeout;
        $this->waitTimeSeconds = $waitTimeSeconds;
    }

    /**
     * @return array{
     *     QueueUrl: string,
     *     MaxNumberOfMessages?: int,
     *     VisibilityTimeout?: int,
     *     WaitTimeSeconds?: int,
     * }
     */
    public function asPayload(): array
    {
        return array_filter([
            'QueueUrl' => $this->queueUrl,
            'MaxNumberOfMessages' => $this->maxNumberOfMessages,
            'VisibilityTimeout' => $this->visibilityTimeout,
            'WaitTimeSeconds' => $this->waitTimeSeconds,
        ], fn ($value) => null !== $value);
    }

    public function accept(SqsClient $client): Generator
    {
        $attributes = $client->receiveMessage($this->asPayload());
        $messages = $attributes['Messages'] ?? [];
        assert(is_array($messages));
        foreach ($messages as $message) {
            $messageId = $message['MessageId'];
            $body = $message['Body'];
            $receiptHandle = $message['ReceiptHandle'];
            $MD5OfBody = $message['MD5OfBody'];

            assert(is_string($messageId));
            assert(is_string($body));
            assert(is_string($receiptHandle));
            assert(is_string($MD5OfBody));

            if (md5($body) !== $MD5OfBody) {
                throw new RuntimeException('bad md5 of body');
            }
            yield new Response(
                $messageId,
                $body,
                $receiptHandle,
            );
        }
    }
}
