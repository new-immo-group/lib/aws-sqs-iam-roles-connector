<?php

declare(strict_types=1);

namespace NewImmoGroup\AwsBroker\Payload\ListQueueTags;

use NewImmoGroup\AwsBroker\Payload\ApiResponse;

final class Response implements ApiResponse
{
    /** @var array<string, string> */
    public array $tags;

    /** @param array<string, string> $tags */
    public function __construct(array $tags)
    {
        $this->tags = $tags;
    }
}
