<?php

declare(strict_types=1);

namespace NewImmoGroup\AwsBroker\Payload\ListQueueTags;

use Aws\Sqs\SqsClient;
use NewImmoGroup\AwsBroker\Payload\ApiRequest;

/**
 * @see https://docs.aws.amazon.com/AWSSimpleQueueService/latest/APIReference/API_ListQueueTags.html
 *
 * @implements ApiRequest<Response>
 */
final class Request implements ApiRequest
{
    public string $queueUrl;

    public function __construct(string $queueUrl)
    {
        $this->queueUrl = $queueUrl;
    }

    /**
     * @return array{QueueUrl: string}
     */
    public function asPayload(): array
    {
        return [
            'QueueUrl' => $this->queueUrl,
        ];
    }

    public function accept(SqsClient $client): Response
    {
        $result = $client->listQueueTags($this->asPayload());
        $tags = $result['Tags'];
        if (!is_array($tags)) {
            $tags = [];
        }

        return new Response($tags);
    }
}
