<?php

declare(strict_types=1);

namespace NewImmoGroup\AwsBroker\Payload\GetQueueAttributes;

use NewImmoGroup\AwsBroker\Payload\ApiResponse;
use NewImmoGroup\AwsBroker\Payload\AttributeDict;

final class Response implements ApiResponse
{
    public AttributeDict $attributes;

    public function __construct(AttributeDict $attributeDict)
    {
        $this->attributes = $attributeDict;
    }
}
