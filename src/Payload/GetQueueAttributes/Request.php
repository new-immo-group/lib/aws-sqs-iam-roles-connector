<?php

declare(strict_types=1);

namespace NewImmoGroup\AwsBroker\Payload\GetQueueAttributes;

use Aws\Sqs\SqsClient;
use NewImmoGroup\AwsBroker\Payload\ApiRequest;
use NewImmoGroup\AwsBroker\Payload\AttributeDict;

/**
 * @see https://docs.aws.amazon.com/AWSSimpleQueueService/latest/APIReference/API_GetQueueAttributes.html
 *
 * @implements ApiRequest<Response>
 *
 * @phpstan-type AttributeKey string
 */
final class Request implements ApiRequest
{
    public string $queueUrl;
    /** @var list<AttributeKey> */
    public array $attributeNames = [];

    /**
     * @param list<AttributeKey> $attributeNames
     */
    public function __construct(string $queueUrl, array $attributeNames)
    {
        $this->queueUrl = $queueUrl;
        $this->attributeNames = $attributeNames;
    }

    /**
     * @return array{
     *     QueueUrl: string,
     *     AttributeNames: list<AttributeKey>,
     * }
     */
    public function asPayload(): array
    {
        return [
            'QueueUrl' => $this->queueUrl,
            'AttributeNames' => $this->attributeNames,
        ];
    }

    public function accept(SqsClient $client): Response
    {
        $attributes = $client->getQueueAttributes($this->asPayload())['Attributes'] ?? [];
        assert(is_array($attributes));
        $dict = new AttributeDict();
        foreach ($attributes as $name => $value) {
            $dict->$name = $value;
        }

        return new Response($dict);
    }
}
