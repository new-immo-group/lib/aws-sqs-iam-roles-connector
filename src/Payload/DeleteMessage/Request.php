<?php

declare(strict_types=1);

namespace NewImmoGroup\AwsBroker\Payload\DeleteMessage;

use Aws\Sqs\SqsClient;
use NewImmoGroup\AwsBroker\Payload\ApiRequest;

/**
 * @see https://docs.aws.amazon.com/AWSSimpleQueueService/latest/APIReference/API_DeleteMessage.html
 *
 * @implements ApiRequest<Response>
 */
final class Request implements ApiRequest
{
    public string $queueUrl;
    public string $receiptHandle;

    public function __construct(string $queueUrl, string $receiptHandle)
    {
        $this->queueUrl = $queueUrl;
        $this->receiptHandle = $receiptHandle;
    }

    /**
     * @return array{
     *     QueueUrl: string,
     *     ReceiptHandle: string,
     * }
     */
    public function asPayload(): array
    {
        return [
            'QueueUrl' => $this->queueUrl,
            'ReceiptHandle' => $this->receiptHandle,
        ];
    }

    public function accept(SqsClient $client): Response
    {
        $client->deleteMessage($this->asPayload());

        return new Response();
    }
}
