<?php

declare(strict_types=1);

namespace NewImmoGroup\AwsBroker\Payload\CreateQueue;

use NewImmoGroup\AwsBroker\Payload\ApiResponse;

final class Response implements ApiResponse
{
    public string $queueUrl;

    public function __construct(string $queueUrl)
    {
        $this->queueUrl = $queueUrl;
    }
}
