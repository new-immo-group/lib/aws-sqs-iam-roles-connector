<?php

declare(strict_types=1);

namespace NewImmoGroup\AwsBroker\Payload\CreateQueue;

use Aws\Sqs\SqsClient;
use NewImmoGroup\AwsBroker\Payload\ApiRequest;
use NewImmoGroup\AwsBroker\Payload\AttributeDict;

/**
 * @see https://docs.aws.amazon.com/AWSSimpleQueueService/latest/APIReference/API_CreateQueue.html
 *
 * @phpstan-import-type AttributeDictPayload from AttributeDict
 *
 * @implements ApiRequest<Response>
 */
final class Request implements ApiRequest
{
    public string $queueName;
    /** @var array<string, string> */
    public array $tags = [];

    public AttributeDict $attributes;

    /**
     * @param string[] $tags
     */
    public function __construct(string $queueName, array $tags, AttributeDict $attributes)
    {
        $this->queueName = $queueName;
        $this->tags = $tags;
        $this->attributes = $attributes;
    }

    /**
     * @return array{
     *     Attributes: AttributeDictPayload,
     *     QueueName: string,
     *     tags: array<string, string>
     * }
     */
    public function asPayload(): array
    {
        return array_filter([
            'Attributes' => $this->attributes->asPayload(),
            'QueueName' => $this->queueName,
            'tags' => $this->tags,
        ], fn ($value) => null !== $value);
    }

    public function accept(SqsClient $client): Response
    {
        $responsePayload = $client->createQueue($this->asPayload());
        $queueUrl = $responsePayload['QueueUrl'];
        assert(is_string($queueUrl));

        return new Response($queueUrl);
    }
}
