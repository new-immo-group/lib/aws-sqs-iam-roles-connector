<?php

declare(strict_types=1);

namespace NewImmoGroup\AwsBroker\Payload;

use Aws\Sqs\SqsClient;
use Generator;
use NewImmoGroup\AwsBroker\Payload\ApiResponse as T;

/**
 * @template T of ApiResponse
 */
interface ApiRequestIterable
{
    /** @return array<string, mixed> */
    public function asPayload(): array;

    /** @return Generator<T> */
    public function accept(SqsClient $client): Generator;
}
