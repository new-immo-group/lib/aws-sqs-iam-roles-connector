<?php

declare(strict_types=1);

namespace NewImmoGroup\AwsBroker\Payload;

use Aws\Sqs\SqsClient;

/**
 * @template T of ApiResponse
 */
interface ApiRequest
{
    /** @return array<string, mixed> */
    public function asPayload(): array;

    /** @return T */
    public function accept(SqsClient $client);
}
