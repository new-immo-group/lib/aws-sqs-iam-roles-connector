<?php

declare(strict_types=1);

namespace NewImmoGroup\AwsBroker;

use Aws\Sqs\Exception\SqsException;
use NewImmoGroup\AwsBroker\Entity\AddressToQueueStamp;
use NewImmoGroup\AwsBroker\Exception\InvalidParameterException;
use NewImmoGroup\AwsBroker\Exception\MissingParameterException;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Exception\TransportException;
use Symfony\Component\Messenger\Stamp\DelayStamp;
use Symfony\Component\Messenger\Stamp\StampInterface;
use Symfony\Component\Messenger\Transport\Sender\SenderInterface;
use Symfony\Component\Messenger\Transport\Serialization\SerializerInterface;
use Throwable;

class SqsSender implements SenderInterface
{
    private SqsAdapter $adapter;
    private SerializerInterface $serializer;

    public function __construct(
        SqsAdapter $adapter,
        SerializerInterface $serializer
    ) {
        $this->adapter = $adapter;
        $this->serializer = $serializer;
    }

    /**
     * @param array<class-string> $removeStampClasses
     *
     * this function exists because Envelope::withoutAll clones everything,
     * and we cannot provide multiple classes to remove.
     *
     * So using Envelope::withoutAll multiple times to exclude multiple classes is heavy
     */
    private static function editStamps(Envelope $envelope, array $removeStampClasses): Envelope
    {
        $stamps = $envelope->all();
        $newStamps = [];

        array_walk_recursive($stamps, function (StampInterface $stamp) use (&$newStamps, $removeStampClasses) {
            foreach ($removeStampClasses as $removedStampClass) {
                if ($stamp instanceof $removedStampClass) {
                    return;
                }
            }
            $newStamps[] = $stamp;
        });

        return new Envelope($envelope->getMessage(), $newStamps);
    }

    /**
     * @throws MissingParameterException when DSN is missing the account ID
     * @throws InvalidParameterException when DSN is not valid
     * @throws TransportException        when message failed to be sent
     * @throws SqsException              when message failed to be re-sent (after queue was automatically created)
     */
    public function send(
        Envelope $envelope
    ): Envelope {
        /** @var list<AddressToQueueStamp|list<AddressToQueueStamp>> $replyToStamps */
        $replyToStamps = $envelope->all(AddressToQueueStamp::class);

        $message = [];

        if (
            ($delay = $envelope->last(DelayStamp::class))
            && $delay instanceof DelayStamp
        ) {
            $delayInMs = $delay->getDelay();
            $message['DelaySeconds'] = ((int) floor($delayInMs / 1000));
        }

        $stamp = $this->getLast($replyToStamps);

        if (null !== $stamp) {
            $message['QueueUrl'] = $this->adapter->getQueueUrlForName($stamp->queueName);
        }

        $envelope = self::editStamps(
            $envelope,
            [AddressToQueueStamp::class, DelayStamp::class]
        );

        $message['MessageBody'] = $this->encode($envelope);

        try {
            $this->adapter->sendMessage($message);
        } catch (SqsException $exception) {
            if ($this->recoverFromQueueNotFoundException($exception, $stamp->queueName ?? null)) {
                $this->adapter->sendMessage($message);
            } else {
                throw new TransportException(
                    $exception->getMessage(),
                    $exception->getCode(),
                    $exception
                );
            }
        }

        return $envelope;
    }

    /**
     * @throws TransportException when Envelope failed to be encoded
     */
    private function encode(
        Envelope $envelope
    ): string {
        try {
            return json_encode(
                $this->serializer->encode($envelope),
                JSON_THROW_ON_ERROR
            );
        } catch (Throwable $exception) {
            throw new TransportException(
                'Failed to encode Envelope',
                $exception->getCode(),
                $exception
            );
        }
    }

    private function recoverFromQueueNotFoundException(
        SqsException $exception,
        ?string $queueName
    ): bool {
        if (
            SqsTransport::isQueueNotFoundException($exception)
            && $this->adapter->hasAutoSetup()
        ) {
            try {
                $args = [];
                if (null !== $queueName) {
                    $args['QueueName'] = $queueName;
                }
                $this->adapter->createQueue($args);

                return true;
            } catch (Throwable $unused) {
                // recovery failed
            }
        }

        return false;
    }

    /**
     * @template T
     *
     * @param list<T|list<T>> $stamps
     *
     * @return ?T
     */
    private static function getLast(array $stamps)
    {
        $last = null;
        array_walk_recursive($stamps, static function ($stamp) use (&$last): void {
            $last = $stamp;
        });

        return $last;
    }
}
