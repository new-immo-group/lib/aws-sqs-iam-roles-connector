<?php

declare(strict_types=1);

namespace NewImmoGroup\AwsBroker\Exception;

use Exception;

class MissingParameterException extends Exception
{
}
