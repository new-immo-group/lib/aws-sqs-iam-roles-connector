<?php

declare(strict_types=1);

namespace NewImmoGroup\AwsBroker\Exception;

use Exception;

class InvalidParameterException extends Exception
{
}
