<?php

declare(strict_types=1);

namespace NewImmoGroup\AwsBroker\Event;

interface EventDispatcherInterface
{
    /** @param array<string, mixed> $message */
    public function messageDecoded(array $message): void;
}
