<?php

declare(strict_types=1);

namespace NewImmoGroup\AwsBroker\Event;

final class NullEventDispatcher implements EventDispatcherInterface
{
    public function messageDecoded(array $message): void
    {
    }
}
