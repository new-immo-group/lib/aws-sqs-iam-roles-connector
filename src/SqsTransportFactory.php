<?php

declare(strict_types=1);

namespace NewImmoGroup\AwsBroker;

use NewImmoGroup\AwsBroker\Entity\Configuration;
use NewImmoGroup\AwsBroker\Event\EventDispatcherInterface;
use NewImmoGroup\AwsBroker\Payload\SqsFacade;
use NewImmoGroup\AwsBroker\Payload\SqsTransportOption;
use NewImmoGroup\AwsBroker\Payload\SqsTransportUsingFacade;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Transport\Serialization\SerializerInterface;
use Symfony\Component\Messenger\Transport\TransportFactoryInterface;
use Symfony\Component\Messenger\Transport\TransportInterface;

/**
 * @phpstan-import-type Options from Configuration
 */
class SqsTransportFactory implements TransportFactoryInterface
{
    private const DSN_STARTS_WITH = 'https://sqs.';

    /**
     * @var Options
     */
    private array $defaultOptions;
    private EventDispatcherInterface $eventDispatcher;
    private LoggerInterface $logger;
    private ArrayCache $cache;

    /** @param Options $defaultOptions */
    public function __construct(
        EventDispatcherInterface $eventDispatcher,
        LoggerInterface $logger,
        array $defaultOptions = [
            'auto_setup' => false,
            'max_message' => 1,
            'queue_prefix' => '',
            'queue_tags' => [],
            'wait_time' => 0,
            'visibility_timeout' => 30,
        ]
    ) {
        $this->defaultOptions = $defaultOptions;
        $this->eventDispatcher = $eventDispatcher;
        $this->logger = $logger;
        $this->cache = new ArrayCache();
    }

    /**
     * @param string  $dsn     which must start with "https://sqs."
     * @param Options $options
     */
    public function createTransport(
        string $dsn,
        array $options,
        SerializerInterface $serializer
    ): TransportInterface {
        $options = array_merge($this->defaultOptions, $options);
        $configuration = new Configuration(
            $dsn,
            $options
        );

        if (true === (bool) ($options['unstable'] ?? false)) {
            /** @phpstan-ignore-next-line */
            $config = SqsTransportOption::from($options);

            return $this->createFacadeTransport($config, $serializer);
        }

        return new SqsTransport(
            new SqsAdapter($configuration, $this->logger),
            $this->eventDispatcher,
            $serializer,
        );
    }

    /**
     * @param array<string, mixed> $options is not used
     */
    public function supports(
        string $dsn,
        array $options
    ): bool {
        return 0 === strpos($dsn, self::DSN_STARTS_WITH);
    }

    public function createFacadeTransport(SqsTransportOption $config, SerializerInterface $serializer): SqsTransportUsingFacade
    {
        return new SqsTransportUsingFacade(
            new SqsFacade(
                $region = SqsFacade::getEnvAsString('AWS_REGION'),
                SqsFacade::getEnvAsString('AWS_VERSION'),
                $accountId = SqsFacade::getEnvAsString('AWS_ACCOUNT_ID'),
                $config->awsClientOptions,
                $this->cache,
                $config->queueName,
                SqsFacade::makeQueueUrl($region, $accountId, $config->queueName),
            ),
            $serializer,
            $this->eventDispatcher,
            $config,
        );
    }
}
