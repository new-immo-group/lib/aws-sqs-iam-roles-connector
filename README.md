# Messenger-SQS

This lib allows a simpler integration of AWS's SQS queues through IAM authentication.

It will register a new Messenger Transport for any DSN matching `^https://sqs.`. 

## Installation

```shell
$ composer require new-immo-group/messenger-transport-sqs-iam
```

## Configuration
### Env variables

These env variables will be required:

```shell
AWS_REGION=                         # in my case, eu-west-3
AWS_VERSION=                        # generically latest
AWS_SDK_LOAD_NONDEFAULT_CONFIG=true # whether to load .aws/config
AWS_PROFILE=                        # profile in $HOME/.aws/config you want to load
AWS_ACCOUNT_ID=
MESSENGER_TRANSPORT_DSN=https://sqs.${AWS_REGION}.amazonaws.com/${AWS_ACCOUNT_ID}
```
### AWS SDK
The files `$HOME/.aws/config` and `$HOME/.aws/credentials` must exists on the host.

Note that if the env variable `AWS_SDK_LOAD_NONDEFAULT_CONFIG` is set,
when loading `.aws/credentials` the AWS-SDK will also load `.aws/config`.

If there is an active `open_basedir()` restriction, make sure to add `$HOME/.aws` to the allowed path list.

### Symfony
#### Register the SQS transport
```yaml
services:
  NewImmoGroup\AwsBroker\SqsTransportFactory:
    class: NewImmoGroup\AwsBroker\SqsTransportFactory
    tags: [messenger.transport_factory]
    arguments:
      $defaultOptions:
        auto_setup: true
        queue_prefix: 'some-prefix-'
        queue_tags:
          queue_env: "%env(APP_ENV)%"
```

We can provide the factory optional default option values:

- `auto_setup`:  It controls, at the Transport level, whether the queue is created automatically when a message is sent. Default to `false`.
- `queue_prefix`: Define a prefix that will be added to the queue name.
- `queue_tags`: A list of tags that will be set on all created queues.

Any of these options can be overridden at the queue level (see below).

#### Queue configuration example
```yaml
framework:
  messenger:
    transports:
      async:
        dsn: '%env(MESSENGER_TRANSPORT_DSN)%'
        serializer: messenger_serializer
        options:
          queue_name: "my-queue-name-here"
          auto_setup: false
          queue_prefix: 'some-prefix-'
          queue_tags:
            env: "%env(APP_ENV)%"
```

The `auto_setup`, `queue_prefix` and `queue_tags` parameters are optional. They override the factory defaults, if any.

## CI/Tests
PHP7.4-CLI and PHP8.1-CLI are required.

The end-to-end/functional test will require a valid AWS configuration (which usually expires after a few hours).

```shell
make test # Symfony5/PHP7.4
make test@php8 # Symfony5/PHP8.1
make test@sf6 # Symfony6/PHP8.1
```

## AWS.SQS Permissions

Provided user should be able to call :

- GetQueueUrl
- CreateQueue
- SendMessage
- DeleteMessage
- GetQueueAttribute

