# Operations

## Queue Creation

This lib aims for idempotent queue creation.

The queue creation detailed will occur on three scenarios :

- Before the first message recieved
- After failing to deliver a message to a queue with an error of type : `AWS.SimpleQueueService.NonExistentQueue`
- Or on `bin/console messenger:setup TheTransport`

The process is detailed here :

```mermaid
flowchart TD
    start[[Start]]
    get-queue-attributes{GetQueueAttributes}
    no-op[No Operation]
    create-queue[CreateQueue]
    queue-exists[Queue exists]
    compare-attributes{Compare attributes}
    set-queue-attributes[
        SetQueueAttributes
        ListQueueTags
        TagQueue
        UntagQueue
    ]

    start -->|flag auto_setup is true| get-queue-attributes
    get-queue-attributes -->|Queue missing KO| create-queue
    get-queue-attributes -->|queue exists OK| queue-exists
    queue-exists -->|flag create_attributes is true| compare-attributes
    compare-attributes -->|Same attributes| no-op
    compare-attributes -->|Different attributes| set-queue-attributes
```

The goal of this process flag is :
- To create a queue when no queue exists.
